module ekipur.graphics.vulkan.tutorial {
  requires org.slf4j;
  requires org.slf4j.simple;
  requires org.joml;
  requires org.lwjgl.glfw;
  requires org.lwjgl.assimp;
  requires org.lwjgl.shaderc;
  requires org.lwjgl.stb;
  requires org.lwjgl.vulkan;
  requires org.lwjgl.natives;
  requires org.lwjgl.assimp.natives;
  requires org.lwjgl.glfw.natives;
  requires org.lwjgl.shaderc.natives;
  requires org.lwjgl.stb.natives;

  opens ekipur.graphics.vulkan.tutorial.resources.naitsirc98.models;
  opens ekipur.graphics.vulkan.tutorial.resources.naitsirc98.shaders;
  opens ekipur.graphics.vulkan.tutorial.resources.naitsirc98.textures;
}
