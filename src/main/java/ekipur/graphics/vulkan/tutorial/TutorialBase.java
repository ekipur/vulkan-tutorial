package ekipur.graphics.vulkan.tutorial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class TutorialBase {
  private static final Logger Logger = LoggerFactory.getLogger("tutorial");

  public static void logInfo(String info) {
    Logger.info(info);
  }

  public static void logWarn(String warn) {
    Logger.warn(warn);
  }

  public static void logError(String error) {
    Logger.error(error);
  }

  public static void logDebug(String msg) {
    Logger.debug(msg);
  }
}
