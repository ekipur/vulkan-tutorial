package ekipur.graphics.vulkan.tutorial;

import java.util.ArrayList;

import ekipur.graphics.vulkan.tutorial.a_setup.*;
import ekipur.graphics.vulkan.tutorial.b_presentation.Ch06SwapChainCreation;
import ekipur.graphics.vulkan.tutorial.b_presentation.Ch07SwapChainImageViews;
import ekipur.graphics.vulkan.tutorial.c_graphicspipelinebasics.*;
import ekipur.graphics.vulkan.tutorial.d_drawing.*;
import ekipur.graphics.vulkan.tutorial.e_swapchainrecreation.Ch16SwapChainRecreation;
import ekipur.graphics.vulkan.tutorial.f_vertexbuffers.*;
import ekipur.graphics.vulkan.tutorial.g_uniformbuffers.Ch22DescriptorSets;
import ekipur.graphics.vulkan.tutorial.h_texturemapping.*;
import ekipur.graphics.vulkan.tutorial.i_depthbuffering.Ch26DepthBuffering;
import ekipur.graphics.vulkan.tutorial.j_modelloading.Ch27ModelLoading;
import ekipur.graphics.vulkan.tutorial.k_mipmapping.Ch28MipMapping;
import ekipur.graphics.vulkan.tutorial.l_multisampling.Ch29Multisampling;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;

public class TutorialMain {
  private final ArrayList<TutorialApp> appList = new ArrayList<>();

  public TutorialMain() {
    appList.add(new Ch00BaseCode());
    appList.add(new Ch01InstanceCreation());
    appList.add(new Ch02WindowSurface());
    appList.add(new Ch03ValidationLayers());
    appList.add(new Ch04PhysicalDeviceSelection());
    appList.add(new Ch05LogicalDevice());
    appList.add(new Ch06SwapChainCreation());
    appList.add(new Ch07SwapChainImageViews());
    appList.add(new Ch08GraphicsPipeline());
    appList.add(new Ch09ShaderModules());
    appList.add(new Ch10FixedFunctions());
    appList.add(new Ch11RenderPasses());
    appList.add(new Ch12GraphicsPipelineComplete());
    appList.add(new Ch13FrameBuffers());
    appList.add(new Ch14CommandBuffers());
    appList.add(new Ch15HelloTriangle());
    appList.add(new Ch16SwapChainRecreation());
    // Ch17 compiles but is expected to run with a failure, so no main method
    appList.add(new Ch18VertexBuffer());
    appList.add(new Ch19StagingBuffer());
    appList.add(new Ch19StagingBufferTransferQueue());
    appList.add(new Ch20IndexBuffer());
    // Ch21 compiles but is expected to dispaly nothing, so no main method
    appList.add(new Ch22DescriptorSets());
    appList.add(new Ch23TextureImage()); // no texture display
    appList.add(new Ch24Sampler()); // no texture display
    appList.add(new Ch25TextureMapping());
    appList.add(new Ch26DepthBuffering());
    appList.add(new Ch27ModelLoading());
    appList.add(new Ch28MipMapping());
    appList.add(new Ch29Multisampling());
  }

  public static void main(String[] args) {
    TutorialMain mainApp = new TutorialMain();

    logInfo(
        "Press ESC to close the current Chapter and proceed with the next Chapter or Ctrl+ESC to stop the Tutorial");

    boolean terminated = false;

    for (TutorialApp app : mainApp.appList) {
      logInfo("## " + app.toString() + " starting ...");
      terminated = app.run();
      logInfo("## " + app.toString() + " OK");
      if (terminated) {
        break;
      }
    }
    logInfo("Tutorial " + (terminated ? "terminated" : "ended"));
  }
}
