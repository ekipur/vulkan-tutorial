package ekipur.graphics.vulkan.tutorial.j_modelloading;

import java.io.File;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import ekipur.graphics.vulkan.tutorial.i_depthbuffering.Ch26DepthBuffering;
import ekipur.graphics.vulkan.tutorial.naitsirc98.ModelLoader;
import ekipur.graphics.vulkan.tutorial.naitsirc98.ModelLoader.Model;
import org.joml.Vector3f;
import org.joml.Vector3fc;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.assimp.Assimp.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch27ModelLoading extends Ch26DepthBuffering {
  private VertexInfo<?> vertexInfo;
  private IndexInfo<?> indexInfo;
  private int indexType = VK_INDEX_TYPE_UINT32;

  public static void main(String[] args) {
    new Ch27ModelLoading().run();
  }

  public Ch27ModelLoading() {}

  @Override
  protected VertexInfo<?> getVertexInfo() {
    return vertexInfo;
  }

  @Override
  protected IndexInfo<?> getIndexInfo() {
    return indexInfo;
  }

  @Override
  protected final void createTextureSampler() {
    super.createTextureSampler();

    logDebug("=> Ch27:createTextureSampler");

    loadModel();
  }

  @Override
  protected String getImageFilename() {
    return "chalet.jpg";
  }

  @Override
  protected int getIndexType() {
    return indexType;
  }

  @Override
  protected final void updateUniformBuffer(int currentImage) {
    try (MemoryStack stack = stackPush()) {
      UniformBufferObject ubo = new UniformBufferObject();

      ubo.getModel().rotate((float) (glfwGetTime() * Math.toRadians(30)), 0.0f, 0.0f, 1.0f);
      ubo.getView().lookAt(2.0f, 2.0f, 2.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
      ubo.getProj()
          .perspective(
              (float) Math.toRadians(45),
              (float) getSwapChainExtent().width() / (float) getSwapChainExtent().height(),
              0.1f,
              10.0f);
      ubo.getProj().m11(ubo.getProj().m11() * -1);

      PointerBuffer data = stack.mallocPointer(1);
      vkMapMemory(
          getLogicalDevice(),
          getUniformBuffersMemory().get(currentImage),
          0,
          UniformBufferObject.SIZEOF,
          0,
          data);
      {
        memcpy(data.getByteBuffer(0, UniformBufferObject.SIZEOF), ubo);
      }
      vkUnmapMemory(getLogicalDevice(), getUniformBuffersMemory().get(currentImage));
    }
  }

  private void loadModel() {
    logInfo(" > Ch27:loadModel");

    String filename = "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/models/chalet.obj";
    URL modelUrl = getClass().getClassLoader().getResource(filename);

    if (modelUrl != null) {
      File modelFile = new File(modelUrl.getFile());

      Model model = ModelLoader.loadModel(modelFile, aiProcess_FlipUVs | aiProcess_DropNormals);

      vertexInfo =
          new VertexInfo<>(
              new ArrayList<>() {
                {
                  final Vector3fc color = new Vector3f(1.0f, 1.0f, 1.0f);
                  final int vertexCount = model.positions.size();

                  for (int i = 0; i < vertexCount; i++) {
                    add(new VertexExt<>(model.positions.get(i), color, model.texCoords.get(i)));
                  }
                }
              });

      indexInfo =
          new IntegerIndexInfo(
              new ArrayList<>() {
                {
                  model.indices.forEach(this::add);
                }
              });
    } else {
      logError("Model file does not exist: " + filename);

      vertexInfo = super.getVertexInfo();
      indexInfo = super.getIndexInfo();
      indexType = super.getIndexType();
    }
  }

  protected class IntegerIndexInfo extends IndexInfo<Integer> {
    public IntegerIndexInfo(List<Integer> indices) {
      super(indices);
    }

    @Override
    protected int getBufferSize() {
      return Integer.BYTES * getIndices().size();
    }

    @Override
    protected void memCopy(ByteBuffer buffer) {
      getIndices().forEach(buffer::putInt);
      buffer.rewind();
    }
  }
}
