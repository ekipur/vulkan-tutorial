package ekipur.graphics.vulkan.tutorial.l_multisampling;

import java.nio.LongBuffer;
import java.util.function.BiConsumer;

import ekipur.graphics.vulkan.tutorial.k_mipmapping.Ch28MipMapping;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkImageMemoryBarrier.Buffer;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.KHRSwapchain.*;
import static org.lwjgl.vulkan.VK10.*;

public final class Ch29Multisampling extends Ch28MipMapping {
  private ImageResource colorResource;
  private int msaaSamples = VK_SAMPLE_COUNT_1_BIT;

  public Ch29Multisampling() {}

  public static void main(String[] args) {
    new Ch29Multisampling().run();
  }

  @Override
  protected void pickPhysicalDevice() {
    super.pickPhysicalDevice();

    logDebug("=> Ch29:pickPhysicalDevice");

    msaaSamples = getMaxUsableSampleCount();
  }

  @Override
  protected void extendMultiSampleStateCreateInfo(
      VkPipelineMultisampleStateCreateInfo multisampling) {
    logDebug(">> Ch29:extendMultiSampleStateCreateInfo");

    multisampling
        .sampleShadingEnable(true)
        // Enable sample shading in the pipeline
        .minSampleShading(0.2f)
        // Min fraction for sample shading; closer to one is smoother
        .rasterizationSamples(msaaSamples);
  }

  @Override
  protected int getRenderPassAttachmentCount() {
    return super.getRenderPassAttachmentCount() + 1;
  }

  @Override
  protected void createRenderPassAttachment(
      int index,
      VkAttachmentDescription attachment,
      VkAttachmentReference attachmentRef,
      VkSubpassDescription.Buffer subpass,
      MemoryStack stack) {
    if (index == 0) {
      logInfo(
          "-> Ch29:createRenderPassAttachment "
              + index
              + " (msaa image, msaaSamples: "
              + msaaSamples
              + ")");

      // MSAA Image
      attachment
          .format(getSwapChainImageFormat())
          .samples(msaaSamples)
          .loadOp(VK_ATTACHMENT_LOAD_OP_CLEAR)
          .storeOp(VK_ATTACHMENT_STORE_OP_STORE)
          .stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE)
          .stencilStoreOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
          .initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
          .finalLayout(VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

      attachmentRef.attachment(index).layout(VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

      subpass
          .pColorAttachments(VkAttachmentReference.calloc(1, stack).put(0, attachmentRef))
          .colorAttachmentCount(1);
    } else if (index == 2) {
      logInfo("-> Ch29:createRenderPassAttachment " + index + " (present image)");

      // Present Image
      attachment
          .format(getSwapChainImageFormat())
          .samples(VK_SAMPLE_COUNT_1_BIT)
          .loadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE)
          .storeOp(VK_ATTACHMENT_STORE_OP_STORE)
          .stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE)
          .stencilStoreOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
          .initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
          .finalLayout(VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

      attachmentRef.attachment(index).layout(VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

      subpass.pResolveAttachments(VkAttachmentReference.calloc(1, stack).put(0, attachmentRef));
    } else {
      super.createRenderPassAttachment(index, attachment, attachmentRef, subpass, stack);
    }
  }

  @Override
  protected void createDepthResources() {
    logDebug("=> Ch26:createDepthResources");

    createColorResources();

    super.createDepthResources();
  }

  @Override
  protected void cleanupUniformBuffers() {
    logDebug("=< Ch29:cleanupUniformBuffers");

    colorResource.cleanup();

    super.cleanupUniformBuffers();
  }

  @Override
  protected boolean onImageMemoryBarrierPostcondition(
      Buffer barrier,
      int oldLayout,
      int newLayout,
      BiConsumer<Integer, Integer> stageMaskConsumer) {
    boolean consumed =
        super.onImageMemoryBarrierPostcondition(barrier, oldLayout, newLayout, stageMaskConsumer);

    if (!consumed
        && oldLayout == VK_IMAGE_LAYOUT_UNDEFINED
        && newLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
      logDebug("=> Ch29:onImageMemoryBarrierPostcondition");

      barrier
          .srcAccessMask(0)
          .dstAccessMask(
              VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

      stageMaskConsumer.accept(
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT);
      consumed = true;
    }
    return consumed;
  }

  @Override
  protected int getDepthResourcesNumSamples() {
    return msaaSamples;
  }

  @Override
  protected final void extendDeviceFeatures(VkPhysicalDeviceFeatures deviceFeatures) {
    super.extendDeviceFeatures(deviceFeatures);

    deviceFeatures.sampleRateShading(true);
  }

  @Override
  protected LongBuffer allocateFramebufferAttachments(MemoryStack stack) {
    return stack.longs(
        colorResource.getImageView(), getDepthResource().getImageView(), VK_NULL_HANDLE);
  }

  @Override
  protected void putSwapChainImageViewToFramebufferAttachments(
      LongBuffer attachments, long imageView) {
    attachments.put(2, imageView);
  }

  private void createColorResources() {
    logInfo(" > Ch29:createColorResources");

    try (MemoryStack stack = stackPush()) {
      LongBuffer pColorImage = stack.mallocLong(1);
      LongBuffer pColorImageMemory = stack.mallocLong(1);

      createImage(
          getSwapChainExtent().width(),
          getSwapChainExtent().height(),
          msaaSamples,
          getSwapChainImageFormat(),
          VK_IMAGE_TILING_OPTIMAL,
          VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
          pColorImage,
          pColorImageMemory);

      long colorImage = pColorImage.get(0);
      long colorImageMemory = pColorImageMemory.get(0);

      colorResource =
          new ImageResource(
              colorImage,
              colorImageMemory,
              createImageView(colorImage, getSwapChainImageFormat(), VK_IMAGE_ASPECT_COLOR_BIT));

      transitionImageLayout(
          colorImage,
          getSwapChainImageFormat(),
          VK_IMAGE_LAYOUT_UNDEFINED,
          VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);
    }
  }

  private int getMaxUsableSampleCount() {
    int sampleCount = VK_SAMPLE_COUNT_1_BIT;

    try (MemoryStack stack = stackPush()) {
      VkPhysicalDeviceProperties physicalDeviceProperties =
          VkPhysicalDeviceProperties.malloc(stack);
      vkGetPhysicalDeviceProperties(getPhysicalDevice(), physicalDeviceProperties);

      int sampleCountFlags =
          physicalDeviceProperties.limits().framebufferColorSampleCounts()
              & physicalDeviceProperties.limits().framebufferDepthSampleCounts();

      if ((sampleCountFlags & VK_SAMPLE_COUNT_64_BIT) != 0) {
        sampleCount = VK_SAMPLE_COUNT_64_BIT;
      } else if ((sampleCountFlags & VK_SAMPLE_COUNT_32_BIT) != 0) {
        sampleCount = VK_SAMPLE_COUNT_32_BIT;
      } else if ((sampleCountFlags & VK_SAMPLE_COUNT_16_BIT) != 0) {
        sampleCount = VK_SAMPLE_COUNT_16_BIT;
      } else if ((sampleCountFlags & VK_SAMPLE_COUNT_8_BIT) != 0) {
        sampleCount = VK_SAMPLE_COUNT_8_BIT;
      } else if ((sampleCountFlags & VK_SAMPLE_COUNT_4_BIT) != 0) {
        sampleCount = VK_SAMPLE_COUNT_4_BIT;
      } else if ((sampleCountFlags & VK_SAMPLE_COUNT_2_BIT) != 0) {
        sampleCount = VK_SAMPLE_COUNT_2_BIT;
      }
    }

    logInfo(" > Ch29:getMaxUsableSampleCount: " + sampleCount);

    return sampleCount;
  }
}
