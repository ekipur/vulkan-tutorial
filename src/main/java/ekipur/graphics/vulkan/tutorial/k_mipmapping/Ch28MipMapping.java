package ekipur.graphics.vulkan.tutorial.k_mipmapping;

import ekipur.graphics.vulkan.tutorial.j_modelloading.Ch27ModelLoading;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch28MipMapping extends Ch27ModelLoading {
  private int mipLevels;

  public Ch28MipMapping() {}

  public static void main(String[] args) {
    new Ch28MipMapping().run();
  }

  @Override
  protected final int getImageInfoUsage() {
    return super.getImageInfoUsage() | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
  }

  @Override
  protected final int getMipLevels() {
    return mipLevels;
  }

  @Override
  protected final void extendTextureSamplerCreateInfo(VkSamplerCreateInfo samplerInfo) {
    logInfo(">> Ch28:extendTextureSamplerCreateInfo");

    samplerInfo
        .minLod(0) // Optional
        .maxLod(getMipLevels())
        .mipLodBias(0); // Optional
  }

  @Override
  protected void onTransitionTextureImageLayout(
      long image, int imageFormat, int width, int height) {
    logInfo(">> Ch28:onTransitionTextureImageLayout (generating mipmaps)");

    generateMipmaps(image, imageFormat, width, height, getMipLevels());
  }

  private double log2(double n) {
    return Math.log(n) / Math.log(2);
  }

  private void generateMipmaps(long image, int imageFormat, int width, int height, int mipLevels) {
    logInfo(" > Ch28:generateMipmaps");

    mipLevels = (int) Math.floor(log2(Math.max(width, height))) + 1;

    try (MemoryStack stack = stackPush()) {
      // Check if image format supports linear blitting
      VkFormatProperties formatProperties = VkFormatProperties.malloc(stack);
      vkGetPhysicalDeviceFormatProperties(getPhysicalDevice(), imageFormat, formatProperties);

      if ((formatProperties.optimalTilingFeatures()
              & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT)
          == 0) {
        throw new RuntimeException("Texture image format does not support linear blitting");
      }

      VkCommandBuffer commandBuffer = beginSingleTimeCommands();

      VkImageMemoryBarrier.Buffer barrier =
          VkImageMemoryBarrier.calloc(1, stack)
              .sType(VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
              .image(image)
              .srcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
              .dstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
              .dstAccessMask(VK_QUEUE_FAMILY_IGNORED);
      barrier
          .subresourceRange()
          .aspectMask(VK_IMAGE_ASPECT_COLOR_BIT)
          .baseArrayLayer(0)
          .layerCount(1)
          .levelCount(1);

      int mipWidth = width;
      int mipHeight = height;

      for (int i = 1; i < mipLevels; i++) {
        barrier.subresourceRange().baseMipLevel(i - 1);
        barrier
            .oldLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
            .newLayout(VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
            .srcAccessMask(VK_ACCESS_TRANSFER_WRITE_BIT)
            .dstAccessMask(VK_ACCESS_TRANSFER_READ_BIT);

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            null,
            null,
            barrier);

        VkImageBlit.Buffer blit = VkImageBlit.calloc(1, stack);
        blit.srcOffsets(0).set(0, 0, 0);
        blit.srcOffsets(1).set(mipWidth, mipHeight, 1);
        blit.srcSubresource()
            .aspectMask(VK_IMAGE_ASPECT_COLOR_BIT)
            .mipLevel(i - 1)
            .baseArrayLayer(0)
            .layerCount(1);
        blit.dstOffsets(0).set(0, 0, 0);
        blit.dstOffsets(1)
            .set(mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1);
        blit.dstSubresource()
            .aspectMask(VK_IMAGE_ASPECT_COLOR_BIT)
            .mipLevel(i)
            .baseArrayLayer(0)
            .layerCount(1);

        vkCmdBlitImage(
            commandBuffer,
            image,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            blit,
            VK_FILTER_LINEAR);

        barrier
            .oldLayout(VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
            .newLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
            .srcAccessMask(VK_ACCESS_TRANSFER_READ_BIT)
            .dstAccessMask(VK_ACCESS_SHADER_READ_BIT);

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            0,
            null,
            null,
            barrier);

        if (mipWidth > 1) {
          mipWidth /= 2;
        }

        if (mipHeight > 1) {
          mipHeight /= 2;
        }
      }

      barrier.subresourceRange().baseMipLevel(mipLevels - 1);
      barrier
          .oldLayout(VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
          .newLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
          .srcAccessMask(VK_ACCESS_TRANSFER_WRITE_BIT)
          .dstAccessMask(VK_ACCESS_SHADER_READ_BIT);

      vkCmdPipelineBarrier(
          commandBuffer,
          VK_PIPELINE_STAGE_TRANSFER_BIT,
          VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
          0,
          null,
          null,
          barrier);

      endSingleTimeCommands(commandBuffer);
    }
  }
}
