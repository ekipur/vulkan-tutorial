package ekipur.graphics.vulkan.tutorial.c_graphicspipelinebasics;

import java.nio.LongBuffer;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkGraphicsPipelineCreateInfo;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch12GraphicsPipelineComplete extends Ch11RenderPasses {
  private long graphicsPipeline;

  public static void main(String[] args) {
    new Ch12GraphicsPipelineComplete().run();
  }

  public Ch12GraphicsPipelineComplete() {}

  @Override
  public void cleanup() {
    cleanupPipeline();

    super.cleanup();
  }

  @Override
  protected void extendGraphicsPipline(
      MemoryStack stack, GraphicsPipelineElements pipelineElements) {
    super.extendGraphicsPipline(stack, pipelineElements);

    logInfo("=> Ch12:extendGraphicsPipeline (graphics pipeline complete)");

    VkGraphicsPipelineCreateInfo.Buffer pipelineInfo =
        VkGraphicsPipelineCreateInfo.calloc(1, stack)
            .sType(VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO)
            .pStages(pipelineElements.shaderStages)
            .pVertexInputState(pipelineElements.vertexInputInfo)
            .pInputAssemblyState(pipelineElements.inputAssembly)
            .pViewportState(pipelineElements.viewportState)
            .pRasterizationState(pipelineElements.rasterizer)
            .pMultisampleState(pipelineElements.multisampling)
            .pDepthStencilState(pipelineElements.depthStencil)
            .pColorBlendState(pipelineElements.colorBlending)
            .layout(getPipelineLayout())
            .renderPass(getRenderPass())
            .subpass(0)
            .basePipelineHandle(VK_NULL_HANDLE)
            .basePipelineIndex(-1);

    LongBuffer pGraphicsPipeline = stack.mallocLong(1);

    if (vkCreateGraphicsPipelines(
            getLogicalDevice(), VK_NULL_HANDLE, pipelineInfo, null, pGraphicsPipeline)
        != VK_SUCCESS) {
      throw new RuntimeException("Failed to create graphics pipeline");
    }

    graphicsPipeline = pGraphicsPipeline.get(0);
  }

  protected final long getGraphicsPipeline() {
    return graphicsPipeline;
  }

  protected final void cleanupPipeline() {
    logInfo(" < Ch12:cleanupPipeline");

    vkDestroyPipeline(getLogicalDevice(), graphicsPipeline, null);
  }
}
