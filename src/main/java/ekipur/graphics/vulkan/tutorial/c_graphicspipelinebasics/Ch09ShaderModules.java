package ekipur.graphics.vulkan.tutorial.c_graphicspipelinebasics;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;

import ekipur.graphics.vulkan.tutorial.naitsirc98.ShaderSPIRVUtils.SPIRV;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkPipelineShaderStageCreateInfo;
import org.lwjgl.vulkan.VkShaderModuleCreateInfo;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static ekipur.graphics.vulkan.tutorial.naitsirc98.ShaderSPIRVUtils.ShaderKind.*;
import static ekipur.graphics.vulkan.tutorial.naitsirc98.ShaderSPIRVUtils.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

import java.util.TreeMap;

public class Ch09ShaderModules extends Ch08GraphicsPipeline {
  public static void main(String[] args) {
    new Ch09ShaderModules().run();
  }

  public Ch09ShaderModules() {}

  @Override
  protected void extendGraphicsPipline(
      MemoryStack stack, GraphicsPipelineElements pipelineElements) {
    TreeMap<ShaderKind, String> shaderSources = pipelineElements.shaderSources;

    if (shaderSources.isEmpty()) {
      logInfo("=> Ch09:extendGraphicsPipeline (09_shader_base)");

      shaderSources.put(
          VERTEX_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/09_shader_base.vert");

      shaderSources.put(
          FRAGMENT_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/09_shader_base.frag");
    }

    super.extendGraphicsPipline(stack, pipelineElements);

    logInfo("=> Ch09:extendGraphicsPipeline (shader stages)");

    // Let's compile the GLSL shaders into SPIR-V at runtime using the shaderc
    // library; check ShaderSPIRVUtils class to see how it can be done
    SPIRV vertShaderSPIRV = compileShaderFile(shaderSources.get(VERTEX_SHADER), VERTEX_SHADER);
    SPIRV fragShaderSPIRV = compileShaderFile(shaderSources.get(FRAGMENT_SHADER), FRAGMENT_SHADER);

    long vertShaderModule = createShaderModule(vertShaderSPIRV.bytecode());
    long fragShaderModule = createShaderModule(fragShaderSPIRV.bytecode());

    ByteBuffer entryPoint = stack.UTF8("main");

    VkPipelineShaderStageCreateInfo vertShaderStageInfo =
        pipelineElements
            .shaderStages
            .get(0)
            .sType(VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO)
            .stage(VK_SHADER_STAGE_VERTEX_BIT)
            .module(vertShaderModule)
            .pName(entryPoint);

    VkPipelineShaderStageCreateInfo fragShaderStageInfo =
        pipelineElements
            .shaderStages
            .get(1)
            .sType(VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO)
            .stage(VK_SHADER_STAGE_FRAGMENT_BIT)
            .module(fragShaderModule)
            .pName(entryPoint);

    pipelineElements.shaderModules.put(VERTEX_SHADER, vertShaderModule);
    pipelineElements.shaderModules.put(FRAGMENT_SHADER, fragShaderModule);
  }

  private long createShaderModule(ByteBuffer spirvCode) {
    try (MemoryStack stack = stackPush()) {
      VkShaderModuleCreateInfo createInfo =
          VkShaderModuleCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO)
              .pCode(spirvCode);

      LongBuffer pShaderModule = stack.mallocLong(1);

      if (vkCreateShaderModule(getLogicalDevice(), createInfo, null, pShaderModule) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create shader module");
      }

      return pShaderModule.get(0);
    }
  }
}
