package ekipur.graphics.vulkan.tutorial.c_graphicspipelinebasics;

import java.nio.LongBuffer;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.KHRSwapchain.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch11RenderPasses extends Ch10FixedFunctions {
  private long renderPass;

  public static void main(String[] args) {
    new Ch11RenderPasses().run();
  }

  public Ch11RenderPasses() {}

  @Override
  protected final void createGraphicsPipeline() {
    logDebug("=> Ch11:createGraphicsPipeline");

    createRenderPass();

    super.createGraphicsPipeline();
  }

  @Override
  protected final void cleanupPipelineLayout() {
    super.cleanupPipelineLayout();

    logDebug("=< Ch11:cleanupPipelineLayout");

    cleanupRenderPass();
  }

  protected final long getRenderPass() {
    return renderPass;
  }

  protected void setRenderPass(long renderPass) {
    this.renderPass = renderPass;
  }

  protected void extendRenderPassInfo(VkRenderPassCreateInfo renderPassInfo, MemoryStack stack) {}

  protected int getRenderPassAttachmentCount() {
    return 1;
  }

  protected void createRenderPassAttachment(
      int index,
      VkAttachmentDescription attachment,
      VkAttachmentReference attachmentRef,
      VkSubpassDescription.Buffer subpass,
      MemoryStack stack) {
    if (index == 0) {
      logInfo(" > Ch11:createRenderPassAttachment " + index + " (color attachment)");

      attachment
          .format(getSwapChainImageFormat())
          .samples(VK_SAMPLE_COUNT_1_BIT)
          .loadOp(VK_ATTACHMENT_LOAD_OP_CLEAR)
          .storeOp(VK_ATTACHMENT_STORE_OP_STORE)
          .stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE)
          .stencilStoreOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
          .initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
          .finalLayout(VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

      attachmentRef.attachment(index).layout(VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

      subpass
          .colorAttachmentCount(1)
          .pColorAttachments(VkAttachmentReference.calloc(1, stack).put(0, attachmentRef));
    }
  }

  protected final void createRenderPass() {
    logInfo(" > Ch11:createRenderPass");

    try (MemoryStack stack = stackPush()) {
      VkAttachmentDescription.Buffer attachments =
          VkAttachmentDescription.calloc(getRenderPassAttachmentCount(), stack);
      VkAttachmentReference.Buffer attachmentRefs =
          VkAttachmentReference.calloc(getRenderPassAttachmentCount(), stack);
      VkSubpassDescription.Buffer subpass =
          VkSubpassDescription.calloc(1, stack).pipelineBindPoint(VK_PIPELINE_BIND_POINT_GRAPHICS);

      for (int i = 0; i < getRenderPassAttachmentCount(); i++) {
        createRenderPassAttachment(i, attachments.get(i), attachmentRefs.get(i), subpass, stack);
      }

      VkRenderPassCreateInfo renderPassInfo =
          VkRenderPassCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO)
              .pAttachments(attachments)
              .pSubpasses(subpass);

      extendRenderPassInfo(renderPassInfo, stack);

      LongBuffer pRenderPass = stack.mallocLong(1);

      if (vkCreateRenderPass(getLogicalDevice(), renderPassInfo, null, pRenderPass) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create render pass");
      }

      setRenderPass(pRenderPass.get(0));
    }
  }

  private void cleanupRenderPass() {
    logInfo(" < Ch11:cleanupRenderPass");

    vkDestroyRenderPass(getLogicalDevice(), renderPass, null);
  }
}
