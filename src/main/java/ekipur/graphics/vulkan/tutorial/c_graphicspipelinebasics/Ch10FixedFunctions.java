package ekipur.graphics.vulkan.tutorial.c_graphicspipelinebasics;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch10FixedFunctions extends Ch09ShaderModules {
  public static void main(String[] args) {
    new Ch10FixedFunctions().run();
  }

  public Ch10FixedFunctions() {}

  @Override
  protected void extendGraphicsPipline(
      MemoryStack stack, GraphicsPipelineElements pipelineElements) {
    super.extendGraphicsPipline(stack, pipelineElements);

    logInfo("=> Ch10:extendGraphicsPipeline (fixed functions)");

    // ===> VERTEX STAGE <===
    pipelineElements.vertexInputInfo.sType(
        VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO);

    // ===> ASSEMBLY STAGE <===
    pipelineElements
        .inputAssembly
        .sType(VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO)
        .topology(VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST)
        .primitiveRestartEnable(false);

    // ===> VIEWPORT & SCISSOR
    VkExtent2D swapChainExtent = getSwapChainExtent();

    VkViewport.Buffer viewport =
        VkViewport.calloc(1, stack)
            .x(0.0f)
            .y(0.0f)
            .width(swapChainExtent.width())
            .height(swapChainExtent.height())
            .minDepth(0.0f)
            .maxDepth(1.0f);

    VkRect2D.Buffer scissor =
        VkRect2D.calloc(1, stack)
            .offset(VkOffset2D.calloc(stack).set(0, 0))
            .extent(swapChainExtent);

    pipelineElements
        .viewportState
        .sType(VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO)
        .pViewports(viewport)
        .pScissors(scissor);

    // ===> RASTERIZATION STAGE <===
    pipelineElements
        .rasterizer
        .sType(VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO)
        .depthClampEnable(false)
        .rasterizerDiscardEnable(false)
        .polygonMode(VK_POLYGON_MODE_FILL)
        .lineWidth(1.0f)
        .cullMode(VK_CULL_MODE_BACK_BIT)
        .frontFace(
            isFrontFaceClockwise() ? VK_FRONT_FACE_CLOCKWISE : VK_FRONT_FACE_COUNTER_CLOCKWISE)
        .depthBiasEnable(false);

    // ===> MULTISAMPLING <===
    extendMultiSampleStateCreateInfo(pipelineElements.multisampling);

    // ===> COLOR BLENDING <===
    VkPipelineColorBlendAttachmentState.Buffer colorBlendAttachment =
        VkPipelineColorBlendAttachmentState.calloc(1, stack)
            .colorWriteMask(
                VK_COLOR_COMPONENT_R_BIT
                    | VK_COLOR_COMPONENT_G_BIT
                    | VK_COLOR_COMPONENT_B_BIT
                    | VK_COLOR_COMPONENT_A_BIT)
            .blendEnable(false);

    pipelineElements
        .colorBlending
        .sType(VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO)
        .logicOpEnable(false)
        .logicOp(VK_LOGIC_OP_COPY)
        .pAttachments(colorBlendAttachment)
        .blendConstants(stack.floats(0.0f, 0.0f, 0.0f, 0.0f));
  }

  protected void extendMultiSampleStateCreateInfo(
      VkPipelineMultisampleStateCreateInfo multisampling) {
    logDebug(">> Ch10:extendMultiSampleStateCreateInfo");

    multisampling
        .sType(VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO)
        .sampleShadingEnable(false)
        .rasterizationSamples(VK_SAMPLE_COUNT_1_BIT);
  }

  protected boolean isFrontFaceClockwise() {
    return true;
  }
}
