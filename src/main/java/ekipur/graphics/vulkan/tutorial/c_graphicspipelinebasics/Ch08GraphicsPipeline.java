package ekipur.graphics.vulkan.tutorial.c_graphicspipelinebasics;

import java.nio.LongBuffer;
import java.util.TreeMap;

import ekipur.graphics.vulkan.tutorial.b_presentation.Ch07SwapChainImageViews;
import ekipur.graphics.vulkan.tutorial.naitsirc98.ShaderSPIRVUtils.ShaderKind;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static ekipur.graphics.vulkan.tutorial.naitsirc98.ShaderSPIRVUtils.ShaderKind.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch08GraphicsPipeline extends Ch07SwapChainImageViews {
  private long pipelineLayout;

  public static void main(String[] args) {
    new Ch08GraphicsPipeline().run();
  }

  public Ch08GraphicsPipeline() {}

  @Override
  public void initVulkan() {
    super.initVulkan();

    logInfo(">> Ch08:initVulkan");

    createPipelineLayout();
    createGraphicsPipeline();
  }

  @Override
  public void cleanup() {
    cleanupPipelineLayout();

    super.cleanup();
  }

  protected void extendGraphicsPipline(
      MemoryStack stack, GraphicsPipelineElements pipelineElements) {}

  protected void createGraphicsPipeline() {
    logInfo(" > Ch08:createGraphicsPipeline");

    GraphicsPipelineElements pipelineElements = new GraphicsPipelineElements();

    try (MemoryStack stack = MemoryStack.stackPush()) {
      // 2 stages: vertex and fragment
      pipelineElements.shaderStages = VkPipelineShaderStageCreateInfo.calloc(2, stack);
      pipelineElements.vertexInputInfo = VkPipelineVertexInputStateCreateInfo.calloc(stack);
      pipelineElements.inputAssembly = VkPipelineInputAssemblyStateCreateInfo.calloc(stack);
      pipelineElements.viewportState = VkPipelineViewportStateCreateInfo.calloc(stack);
      pipelineElements.rasterizer = VkPipelineRasterizationStateCreateInfo.calloc(stack);
      pipelineElements.multisampling = VkPipelineMultisampleStateCreateInfo.calloc(stack);
      pipelineElements.depthStencil = VkPipelineDepthStencilStateCreateInfo.calloc(stack);
      pipelineElements.colorBlending = VkPipelineColorBlendStateCreateInfo.calloc(stack);

      extendGraphicsPipline(stack, pipelineElements);

      if (!pipelineElements.shaderModules.isEmpty()) {
        vkDestroyShaderModule(
            getLogicalDevice(), pipelineElements.shaderModules.get(VERTEX_SHADER), null);
        vkDestroyShaderModule(
            getLogicalDevice(), pipelineElements.shaderModules.get(FRAGMENT_SHADER), null);
      }
    }
  }

  protected void extendPipelineLayoutInfo(VkPipelineLayoutCreateInfo info) {}

  protected final void createPipelineLayout() {
    logInfo(" > Ch08:createPipelineLayout");

    try (MemoryStack stack = MemoryStack.stackPush()) {
      VkPipelineLayoutCreateInfo pipelineLayoutInfo =
          VkPipelineLayoutCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO);

      extendPipelineLayoutInfo(pipelineLayoutInfo);

      LongBuffer pPipelineLayout = stack.longs(VK_NULL_HANDLE);

      if (vkCreatePipelineLayout(getLogicalDevice(), pipelineLayoutInfo, null, pPipelineLayout)
          != VK_SUCCESS) {
        throw new RuntimeException("Failed to create pipeline layout");
      }

      pipelineLayout = pPipelineLayout.get(0);
    }
  }

  protected final long getPipelineLayout() {
    return pipelineLayout;
  }

  protected void cleanupPipelineLayout() {
    logInfo(" < Ch08:cleanupPipelineLayout");

    vkDestroyPipelineLayout(getLogicalDevice(), pipelineLayout, null);
  }

  public class GraphicsPipelineElements {
    public final TreeMap<ShaderKind, String> shaderSources = new TreeMap<>();
    public final TreeMap<ShaderKind, Long> shaderModules = new TreeMap<>();
    public VkPipelineShaderStageCreateInfo.Buffer shaderStages;
    public VkPipelineVertexInputStateCreateInfo vertexInputInfo;
    public VkPipelineInputAssemblyStateCreateInfo inputAssembly;
    public VkPipelineViewportStateCreateInfo viewportState;
    public VkPipelineRasterizationStateCreateInfo rasterizer;
    public VkPipelineMultisampleStateCreateInfo multisampling;
    public VkPipelineDepthStencilStateCreateInfo depthStencil;
    public VkPipelineColorBlendStateCreateInfo colorBlending;
  }
}
