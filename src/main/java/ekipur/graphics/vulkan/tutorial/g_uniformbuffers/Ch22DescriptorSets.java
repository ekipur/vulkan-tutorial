package ekipur.graphics.vulkan.tutorial.g_uniformbuffers;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static ekipur.graphics.vulkan.tutorial.naitsirc98.AlignmentUtils.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch22DescriptorSets extends Ch21DescriptorLayout {
  private long descriptorPool;
  private List<Long> descriptorSets;

  public static void main(String[] args) {
    new Ch22DescriptorSets().run();
  }

  public Ch22DescriptorSets() {}

  @Override
  protected boolean isWindowResizable() {
    return super.isWindowResizable();
    // return true; // stottert bei Größenänderung des Fensters
  }

  @Override
  protected boolean isFrontFaceClockwise() {
    return false;
  }

  @Override
  protected void createUniformBuffers() {
    super.createUniformBuffers();

    logDebug("=> Ch22:createUniformBuffers");

    createDescriptorPool();
    createDescriptorSets();
  }

  @Override
  protected void cleanupUniformBuffers() {
    super.cleanupUniformBuffers();

    logDebug("=< Ch22:cleanupUniformBuffers");

    cleanupDescriptorPool();
  }

  @Override
  protected final void bindDescriptorSets(VkCommandBuffer commandBuffer, int commandBufferIndex) {
    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        getPipelineLayout(),
        0,
        stackGet().longs(descriptorSets.get(commandBufferIndex)),
        null);
  }

  @Override
  protected final void memcpy(ByteBuffer buffer, UniformBufferObject ubo) {
    final int mat4Size = 16 * Float.BYTES;

    ubo.getModel().get(0, buffer);
    ubo.getView().get(alignas(mat4Size, alignof(ubo.getView())), buffer);
    ubo.getProj().get(alignas(mat4Size * 2, alignof(ubo.getView())), buffer);
  }

  protected int getDescriptorPoolSizeCount() {
    return 1;
  }

  protected boolean updateDescriptorPoolSize(int index, VkDescriptorPoolSize poolSize) {
    if (index == 0) {
      logInfo(" > Ch22:updateDescriptorPoolSize " + index + " (uniform buffer)");

      poolSize.type(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
      poolSize.descriptorCount(getSwapChainImages().size());
      return true;
    }
    return false;
  }

  private void cleanupDescriptorPool() {
    logInfo(" < Ch22:cleanupDescriptorPool");

    vkDestroyDescriptorPool(getLogicalDevice(), descriptorPool, null);
  }

  private void createDescriptorPool() {
    logInfo(" > Ch22:createDescriptorPool");

    try (MemoryStack stack = stackPush()) {
      int poolSizeCount = getDescriptorPoolSizeCount();
      VkDescriptorPoolSize.Buffer poolSizes = VkDescriptorPoolSize.calloc(poolSizeCount, stack);

      for (int index = 0; index < poolSizeCount; index++) {
        updateDescriptorPoolSize(index, poolSizes.get(index));
      }

      VkDescriptorPoolCreateInfo poolInfo =
          VkDescriptorPoolCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO)
              .pPoolSizes(poolSizes)
              .maxSets(getSwapChainImages().size());

      LongBuffer pDescriptorPool = stack.mallocLong(1);

      if (vkCreateDescriptorPool(getLogicalDevice(), poolInfo, null, pDescriptorPool)
          != VK_SUCCESS) {
        throw new RuntimeException("Failed to create descriptor pool");
      }

      descriptorPool = pDescriptorPool.get(0);
    }
  }

  protected int getDescriptorWriteCount() {
    return 1;
  }

  protected boolean fillWriteDescriptorSet(
      int index, VkWriteDescriptorSet descriptorWrite, MemoryStack stack) {
    if (index == 0) {
      logInfo(" > Ch22:fillWriteDescriptorSet " + index + " (uniform buffer)");

      VkDescriptorBufferInfo.Buffer bufferInfo =
          VkDescriptorBufferInfo.calloc(1, stack).offset(0).range(UniformBufferObject.SIZEOF);

      descriptorWrite
          .sType(VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
          .dstBinding(0)
          .dstArrayElement(0)
          .descriptorType(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
          .descriptorCount(1)
          .pBufferInfo(bufferInfo);
      return true;
    }
    return false;
  }

  protected void createDescriptorSets() {
    logInfo(" > Ch22:createDescriptorSets");

    try (MemoryStack stack = stackPush()) {
      int size = getSwapChainImages().size();
      LongBuffer layouts = stack.mallocLong(size);

      for (int i = 0; i < layouts.capacity(); i++) {
        layouts.put(i, getDescriptorSetLayout());
      }

      VkDescriptorSetAllocateInfo allocInfo =
          VkDescriptorSetAllocateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO)
              .descriptorPool(descriptorPool)
              .pSetLayouts(layouts);

      LongBuffer pDescriptorSets = stack.mallocLong(size);

      if (vkAllocateDescriptorSets(getLogicalDevice(), allocInfo, pDescriptorSets) != VK_SUCCESS) {
        throw new RuntimeException("Failed to allocate descriptor sets");
      }

      descriptorSets = new ArrayList<>(pDescriptorSets.capacity());

      int descriptorWriteCount = getDescriptorWriteCount();

      VkWriteDescriptorSet.Buffer descriptorWrites =
          VkWriteDescriptorSet.calloc(descriptorWriteCount, stack);

      for (int i = 0; i < descriptorWriteCount; i++) {
        VkWriteDescriptorSet descriptorWrite = descriptorWrites.get(i);

        if (!fillWriteDescriptorSet(i, descriptorWrite, stack)) {
          logWarn("WriteDescriptorSet " + i + " not updated!");
        }
      }

      for (int j = 0; j < pDescriptorSets.capacity(); j++) {
        long descriptorSet = pDescriptorSets.get(j);

        descriptorWrites.get(0).pBufferInfo().buffer(getUniformBuffers().get(j));

        for (int i = 0; i < descriptorWriteCount; i++) {
          VkWriteDescriptorSet descriptorWrite = descriptorWrites.get(i);

          descriptorWrite.dstSet(descriptorSet);
        }

        vkUpdateDescriptorSets(getLogicalDevice(), descriptorWrites, null);

        descriptorSets.add(descriptorSet);
      }
    }
  }
}
