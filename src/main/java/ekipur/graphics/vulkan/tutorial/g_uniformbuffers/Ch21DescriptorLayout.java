package ekipur.graphics.vulkan.tutorial.g_uniformbuffers;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.List;

import ekipur.graphics.vulkan.tutorial.f_vertexbuffers.Ch20IndexBuffer;
import org.joml.Matrix4f;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static ekipur.graphics.vulkan.tutorial.naitsirc98.ShaderSPIRVUtils.ShaderKind.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public abstract class Ch21DescriptorLayout extends Ch20IndexBuffer {
  private long descriptorSetLayout;
  private List<Long> uniformBuffers;
  private List<Long> uniformBuffersMemory;

  public Ch21DescriptorLayout() {}

  @Override
  protected boolean isWindowResizable() {
    return false;
  }

  @Override
  protected void extendGraphicsPipline(
      MemoryStack stack, GraphicsPipelineElements pipelineElements) {
    if (pipelineElements.shaderSources.isEmpty()) {
      logInfo("=> Ch21:extendGraphicsPipeline (21_shader_ubo)");

      pipelineElements.shaderSources.put(
          VERTEX_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/21_shader_ubo.vert");

      pipelineElements.shaderSources.put(
          FRAGMENT_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/21_shader_ubo.frag");
    }

    super.extendGraphicsPipline(stack, pipelineElements);
  }

  @Override
  protected void extendPipelineLayoutInfo(VkPipelineLayoutCreateInfo info) {
    super.extendPipelineLayoutInfo(info);

    logDebug("=> Ch21:extendPipelineLayoutInfo");

    info.pSetLayouts(MemoryStack.stackGet().longs(descriptorSetLayout));
  }

  @Override
  protected final void createCommandBuffers() {
    logDebug("=> Ch21:createCommandBuffers");

    createUniformBuffers();

    super.createCommandBuffers();
  }

  @Override
  protected void createVertexBuffer() {
    super.createVertexBuffer();

    logDebug("=> Ch21:createVertexBuffer");

    createDescriptorSetLayout();
  }

  @Override
  protected final void cleanupSwapChainRecreation() {
    logDebug("=< Ch21:cleanupSwapChainRecreation");

    cleanupUniformBuffers();

    super.cleanupSwapChainRecreation();
  }

  @Override
  protected void cleanupVertexBuffer() {
    logDebug("=< Ch21:cleanupVertexBuffer");

    cleanupDescriptorSetLayout();

    super.cleanupVertexBuffer();
  }

  @Override
  protected void onDrawFrameUpdate(int imageIndex) {
    updateUniformBuffer(imageIndex);
  }

  protected final long getDescriptorSetLayout() {
    return descriptorSetLayout;
  }

  protected final List<Long> getUniformBuffers() {
    return uniformBuffers;
  }

  protected final List<Long> getUniformBuffersMemory() {
    return uniformBuffersMemory;
  }

  protected void cleanupUniformBuffers() {
    logInfo(" < Ch21:cleanupUniformBuffers");

    uniformBuffers.forEach(ubo -> vkDestroyBuffer(getLogicalDevice(), ubo, null));
    uniformBuffersMemory.forEach(uboMemory -> vkFreeMemory(getLogicalDevice(), uboMemory, null));
  }

  protected void cleanupDescriptorSetLayout() {
    logInfo(" < Ch21:cleanupDescriptorSetLayout");

    vkDestroyDescriptorSetLayout(getLogicalDevice(), descriptorSetLayout, null);
  }

  protected void createUniformBuffers() {
    logInfo(" > Ch21:createUniformBuffers");

    try (MemoryStack stack = stackPush()) {
      int size = getSwapChainImages().size();
      uniformBuffers = new ArrayList<>(size);
      uniformBuffersMemory = new ArrayList<>(size);

      LongBuffer pBuffer = stack.mallocLong(1);
      LongBuffer pBufferMemory = stack.mallocLong(1);

      for (int i = 0; i < size; i++) {
        createBuffer(
            UniformBufferObject.SIZEOF,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            pBuffer,
            pBufferMemory);

        uniformBuffers.add(pBuffer.get(0));
        uniformBuffersMemory.add(pBufferMemory.get(0));
      }
    }
  }

  protected int getDescriptorSetLayoutBindingCount() {
    return 1;
  }

  protected boolean setDescriptorSetLayoutBinding(int index, VkDescriptorSetLayoutBinding binding) {
    if (index == 0) {
      logInfo(" > Ch21:setDescriptorSetLayoutBinding " + index);

      binding
          .binding(index)
          .descriptorCount(1)
          .descriptorType(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER)
          .pImmutableSamplers(null)
          .stageFlags(VK_SHADER_STAGE_VERTEX_BIT);
      return true;
    }
    return false;
  }

  protected void memcpy(ByteBuffer buffer, UniformBufferObject ubo) {
    final int mat4Size = 16 * Float.BYTES;

    ubo.getModel().get(0, buffer);
    ubo.getView().get(mat4Size, buffer);
    ubo.getProj().get(mat4Size * 2, buffer);
  }

  private void createDescriptorSetLayout() {
    logInfo(" > Ch21:createDescriptorSetLayout");

    try (MemoryStack stack = stackPush()) {
      int bindingCount = getDescriptorSetLayoutBindingCount();
      VkDescriptorSetLayoutBinding.Buffer bindings =
          VkDescriptorSetLayoutBinding.calloc(bindingCount, stack);

      for (int index = 0; index < bindingCount; index++) {
        setDescriptorSetLayoutBinding(index, bindings.get(index));
      }

      VkDescriptorSetLayoutCreateInfo layoutInfo =
          VkDescriptorSetLayoutCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO)
              .pBindings(bindings);

      LongBuffer pDescriptorSetLayout = stack.mallocLong(1);

      if (vkCreateDescriptorSetLayout(getLogicalDevice(), layoutInfo, null, pDescriptorSetLayout)
          != VK_SUCCESS) {
        throw new RuntimeException("Failed to create descriptor set layout");
      }
      descriptorSetLayout = pDescriptorSetLayout.get(0);
    }
  }

  protected void updateUniformBuffer(int currentImage) {
    try (MemoryStack stack = stackPush()) {
      UniformBufferObject ubo = new UniformBufferObject();

      // Coord-System: x+ right, y+ down, z+ into screen
      ubo.getModel().rotate((float) (glfwGetTime() * Math.toRadians(-30)), 0.0f, 0.0f, 1.0f);
      ubo.getView().setTranslation(0, 0, -2.5f).rotate((float) Math.toRadians(60), 1, 0, 0);

      ubo.getProj()
          .setPerspective(
              (float) Math.toRadians(45),
              getSwapChainExtent().width() / (float) getSwapChainExtent().height(),
              0.1f,
              10.0f);

      PointerBuffer data = stack.mallocPointer(1);
      vkMapMemory(
          getLogicalDevice(),
          uniformBuffersMemory.get(currentImage),
          0,
          UniformBufferObject.SIZEOF,
          0,
          data);
      {
        memcpy(data.getByteBuffer(0, UniformBufferObject.SIZEOF), ubo);
      }
      vkUnmapMemory(getLogicalDevice(), uniformBuffersMemory.get(currentImage));
    }
  }

  protected class UniformBufferObject {
    public static final int SIZEOF = 3 * 16 * Float.BYTES;

    private Matrix4f model;
    private Matrix4f view;
    private Matrix4f proj;

    public UniformBufferObject() {
      model = new Matrix4f();
      view = new Matrix4f();
      proj = new Matrix4f();
    }

    public Matrix4f getModel() {
      return model;
    }

    public Matrix4f getView() {
      return view;
    }

    public Matrix4f getProj() {
      return proj;
    }
  }
}
