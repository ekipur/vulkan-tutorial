package ekipur.graphics.vulkan.tutorial;

public interface TutorialApp {
  int WIDTH = 800;
  int HEIGHT = 600;

  default boolean run() {
    initWindow();
    initVulkan();
    boolean stopTutorial = mainLoop();
    cleanup();
    return stopTutorial;
  }

  void initWindow();

  void initVulkan();

  boolean mainLoop();

  void cleanup();
}
