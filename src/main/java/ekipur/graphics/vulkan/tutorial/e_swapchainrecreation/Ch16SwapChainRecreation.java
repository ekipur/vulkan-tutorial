package ekipur.graphics.vulkan.tutorial.e_swapchainrecreation;

import java.nio.IntBuffer;
import java.util.List;

import ekipur.graphics.vulkan.tutorial.d_drawing.Ch15HelloTriangle;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.Pointer;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.KHRSwapchain.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch16SwapChainRecreation extends Ch15HelloTriangle {
  private boolean framebufferResize;

  public static void main(String[] args) {
    new Ch16SwapChainRecreation().run();
  }

  public Ch16SwapChainRecreation() {}

  @Override
  protected boolean isWindowResizable() {
    return true;
  }

  @Override
  public void initWindow() {
    super.initWindow();

    logDebug("=> Ch16:initWindow");

    // In Java, we don't really need a user pointer here, because
    // we can simply pass an instance method reference to
    // glfwSetFramebufferSizeCallback
    // However, I will show you how can you pass a user pointer to glfw in Java
    // just for learning purposes:
    // long userPointer = JNINativeInterface.NewGlobalRef(this);
    // glfwSetWindowUserPointer(window, userPointer);
    // Please notice that the reference must be freed manually with
    // JNINativeInterface.nDeleteGlobalRef
    glfwSetFramebufferSizeCallback(getWindowHandle(), this::framebufferResizeCallback);
  }

  @Override
  public void initVulkan() {
    logInfo(">> Ch16:initVulkan");

    createInstance();
    setupDebugMessenger();
    createSurface();
    pickPhysicalDevice();
    createLogicalDevice();
    createCommandPool();
    createVertexBuffer();
    createSwapChainObjects();
    createSyncObjects();
  }

  protected void createVertexBuffer() {}

  protected void cleanupVertexBuffer() {}

  protected void cleanupSwapChainRecreation() {
    logInfo(" < Ch16:cleanupSwapChainRecreation");

    getSwapChainFramebuffers()
        .forEach(framebuffer -> vkDestroyFramebuffer(getLogicalDevice(), framebuffer, null));

    vkFreeCommandBuffers(
        getLogicalDevice(), getCommandPool(), asPointerBuffer(getCommandBuffers()));

    cleanupPipeline();
    cleanupPipelineLayout();
    cleanupSwapChainImageViews();
    cleanupSwapChain();
  }

  @Override
  public void cleanup() {
    cleanupSwapChainRecreation();
    cleanupVertexBuffer();
    cleanupSyncObjects();
    cleanupCommandPool();
    cleanupLogicalDevice();
    cleanupDebugMessenger();
    cleanupSurface();
    cleanupInstance();
    cleanupGLFW();
  }

  @Override
  protected boolean onAcquireNextImageKHR(int vkResult) {
    if (vkResult == VK_ERROR_OUT_OF_DATE_KHR) {
      recreateSwapChain();
      return false;
    }
    return true;
  }

  @Override
  protected void onQueuePresentKHR(int vkResult) {
    if (vkResult == VK_ERROR_OUT_OF_DATE_KHR
        || vkResult == VK_SUBOPTIMAL_KHR
        || isFramebufferResize()) {
      framebufferResize = false;
      recreateSwapChain();
    } else if (vkResult != VK_SUCCESS) {
      throw new RuntimeException("Failed to present swap chain image");
    }
  }

  protected boolean isFramebufferResize() {
    return framebufferResize;
  }

  protected final void recreateSwapChain() {
    logInfo(" > Ch16:recreateSwapChain");

    try (MemoryStack stack = stackPush()) {
      IntBuffer width = stack.ints(0);
      IntBuffer height = stack.ints(0);

      while (width.get(0) == 0 && height.get(0) == 0) {
        glfwGetFramebufferSize(getWindowHandle(), width, height);
        glfwWaitEvents();
      }
    }

    vkDeviceWaitIdle(getLogicalDevice());

    cleanupSwapChain();
    createSwapChainObjects();
  }

  private void framebufferResizeCallback(long window, int width, int height) {
    // HelloTriangleApplication app =
    // MemoryUtil.memGlobalRefToObject(glfwGetWindowUserPointer(window));
    // app.framebufferResize = true;
    framebufferResize = true;
  }

  private void createSwapChainObjects() {
    logInfo(" > Ch16:createSwapChainObjects");

    createSwapChain();
    createSwapChainImageViews();
    createPipelineLayout();
    createGraphicsPipeline();
    createFramebuffers();
    createCommandBuffers();
  }

  private PointerBuffer asPointerBuffer(List<? extends Pointer> list) {
    MemoryStack stack = stackGet();

    PointerBuffer buffer = stack.mallocPointer(list.size());

    list.forEach(buffer::put);

    return buffer.rewind();
  }
}
