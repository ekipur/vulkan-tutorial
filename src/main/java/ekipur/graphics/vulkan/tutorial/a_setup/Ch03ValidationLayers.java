package ekipur.graphics.vulkan.tutorial.a_setup;

import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.*;

import ekipur.graphics.vulkan.tutorial.TutorialBase;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static java.util.stream.Collectors.*;
import static org.lwjgl.glfw.GLFWVulkan.*;
import static org.lwjgl.system.MemoryUtil.*;
import static org.lwjgl.vulkan.EXTDebugUtils.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch03ValidationLayers extends Ch02WindowSurface {
  private final Set<String> validationLayers = new HashSet<>();
  private long debugMessenger;

  public static void main(String[] args) {
    new Ch03ValidationLayers().run();
  }

  public Ch03ValidationLayers() {
    validationLayers.add("VK_LAYER_KHRONOS_validation");
    validationLayers.add("VK_LAYER_LUNARG_standard_validation");
  }

  @Override
  public void initVulkan() {
    super.initVulkan();

    logDebug("=> Ch03:initVulkan");

    setupDebugMessenger();
  }

  @Override
  public void cleanup() {
    cleanupDebugMessenger();

    super.cleanup();
  }

  @Override
  protected boolean extendInstanceCreateInfo(VkInstanceCreateInfo createInfo) {
    logInfo(">> Ch03:extendInstanceCreateInfo (checked validation layer support)");

    if (isValidationLayersRequired() && !checkValidationLayerSupport()) {
      return false;
    }

    createInfo.ppEnabledExtensionNames(getRequiredExtensions());

    if (isValidationLayersRequired()) {
      createInfo.ppEnabledLayerNames(asPointerBuffer(validationLayers));

      VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo =
          VkDebugUtilsMessengerCreateInfoEXT.calloc(MemoryStack.stackGet());
      populateDebugMessengerCreateInfo(debugCreateInfo);
      createInfo.pNext(debugCreateInfo.address());
    }
    return true;
  }

  protected final void cleanupDebugMessenger() {
    if (isValidationLayersRequired()) {
      logInfo(" < Ch03:cleanupDebugMessenger");

      destroyDebugUtilsMessengerEXT(getInstance(), debugMessenger, null);
    }
  }

  protected final long getDebugMessenger() {
    return debugMessenger;
  }

  protected final Collection<String> getValidationLayers() {
    return validationLayers;
  }

  protected final boolean isValidationLayersRequired() {
    return !validationLayers.isEmpty();
  }

  private boolean checkValidationLayerSupport() {
    try (MemoryStack stack = MemoryStack.stackPush()) {
      IntBuffer layercount = stack.ints(0);

      vkEnumerateInstanceLayerProperties(layercount, null);

      VkLayerProperties.Buffer availableLayers = VkLayerProperties.malloc(layercount.get(0), stack);

      vkEnumerateInstanceLayerProperties(layercount, availableLayers);

      final Set<String> availableLayerNames =
          availableLayers.stream().map(VkLayerProperties::layerNameString).collect(toSet());

      logInfo("-- Available Layers:");

      availableLayerNames.forEach(TutorialBase::logInfo);

      // TODO Warum ist VK_LAYER_KHRONOS_validation nicht verfügbar?
      /*
       * Von mir hinzugefügt, da zum Zeitpunkt der Projektbearbeitung (1. Okt 2020) die
       * Validationsschicht VK_LAYER_KHRONOS_validation aus noch unbekanntem Grund nicht
       * verfügbar ist. Jedoch kann zu diesem Zeitpunkt die als veraltet gekennzeichnete Schicht
       * VK_LAYER_LUNARG_standard_validation verwendet werden. Deshalb sind beide Schichten in
       * VALIDATION_LAYERS eingetragen. Da aber nur verfügbare Schichten in VALIDATION_LAYERS
       * enthalten sein dürfen, wird an dieser Stelle entsprechend gefiltert:
       */
      validationLayers.removeIf(
          layer -> {
            if (!availableLayerNames.contains(layer)) {
              logWarn(layer + " not available!");
              return true;
            }
            return false;
          });

      return !validationLayers.isEmpty();
    }
  }

  private int debugCallBack(
      int messageSeverity, int messageType, long pCallbackData, long pUserDate) {
    VkDebugUtilsMessengerCallbackDataEXT callbackData =
        VkDebugUtilsMessengerCallbackDataEXT.create(pCallbackData);

    logError("Validation Layer: " + callbackData.pMessageString());

    return VK_FALSE;
  }

  private int createDebugUtilsMessengerEXT(
      VkInstance instance,
      VkDebugUtilsMessengerCreateInfoEXT createInfo,
      VkAllocationCallbacks allocationCallbacks,
      LongBuffer pDebugMessenger) {
    if (vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT") != NULL) {
      return vkCreateDebugUtilsMessengerEXT(
          instance, createInfo, allocationCallbacks, pDebugMessenger);
    }
    return VK_ERROR_EXTENSION_NOT_PRESENT;
  }

  protected void destroyDebugUtilsMessengerEXT(
      VkInstance instance, long debugMessenger, VkAllocationCallbacks allocationCallbacks) {
    if (vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT") != NULL) {
      vkDestroyDebugUtilsMessengerEXT(instance, debugMessenger, allocationCallbacks);
    }
  }

  private void populateDebugMessengerCreateInfo(
      VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo) {
    debugCreateInfo.sType(VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT);
    debugCreateInfo.messageSeverity(
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT);
    debugCreateInfo.messageType(
        VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT);
    debugCreateInfo.pfnUserCallback(this::debugCallBack);
  }

  protected void setupDebugMessenger() {
    if (!isValidationLayersRequired()) {
      return;
    }

    try (MemoryStack stack = MemoryStack.stackPush()) {
      VkDebugUtilsMessengerCreateInfoEXT createInfo =
          VkDebugUtilsMessengerCreateInfoEXT.calloc(stack);
      populateDebugMessengerCreateInfo(createInfo);

      LongBuffer pDebugMessenger = stack.longs(VK_NULL_HANDLE);

      if (createDebugUtilsMessengerEXT(getInstance(), createInfo, null, pDebugMessenger)
          != VK_SUCCESS) {
        throw new RuntimeException("Failed to set up debug messenger.");
      }

      debugMessenger = pDebugMessenger.get(0);
    }
  }

  protected PointerBuffer asPointerBuffer(Collection<String> collection) {
    MemoryStack stack = MemoryStack.stackGet();
    PointerBuffer buffer = stack.mallocPointer(collection.size());

    collection.stream().map(stack::UTF8).forEach(buffer::put);

    return buffer.rewind();
  }

  private PointerBuffer getRequiredExtensions() {
    PointerBuffer glfwExtensions = glfwGetRequiredInstanceExtensions();

    if (glfwExtensions == null) {
      throw new RuntimeException(
          "Vulkan itself or any required instance extension is not available on this machine!");
    }

    if (isValidationLayersRequired()) {
      MemoryStack stack = MemoryStack.stackGet();
      PointerBuffer extensions = stack.mallocPointer(glfwExtensions.capacity() + 1);

      extensions.put(glfwExtensions);
      extensions.put(stack.UTF8(VK_EXT_DEBUG_UTILS_EXTENSION_NAME));

      // Infoausgabe zusätzlich zum Tutorial:
      logInfo("-- Required Extensions:");
      extensions.rewind();
      for (int i = 0; i < extensions.limit(); i++) {
        logInfo(extensions.getStringUTF8(i));
      }

      return extensions.rewind();
    }
    return glfwExtensions;
  }
}
