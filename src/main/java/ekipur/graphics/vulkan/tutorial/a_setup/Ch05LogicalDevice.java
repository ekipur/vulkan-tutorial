package ekipur.graphics.vulkan.tutorial.a_setup;

import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch05LogicalDevice extends Ch04PhysicalDeviceSelection {
  private VkDevice logicalDevice;
  private VkQueue graphicsQueue;
  private QueueGraphicsFamilyIndices familyIndices;

  public static void main(String[] args) {
    new Ch05LogicalDevice().run();
  }

  public Ch05LogicalDevice() {}

  @Override
  public void initVulkan() {
    super.initVulkan();

    logDebug("=> Ch05:initVulkan");

    createLogicalDevice();
  }

  @Override
  public void cleanup() {
    cleanupLogicalDevice();

    super.cleanup();
  }

  protected final void cleanupLogicalDevice() {
    logInfo(" < Ch05:cleanupLogicalDevice");

    // Device queues are implicitly cleaned up when the device is destroyed, see
    // https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Logical_device_and_queues
    vkDestroyDevice(logicalDevice, null);
  }

  protected final void setLogicalDevice(VkDevice device) {
    logicalDevice = device;
  }

  protected final VkDevice getLogicalDevice() {
    return logicalDevice;
  }

  protected final void setGraphicsQueue(VkQueue queue) {
    graphicsQueue = queue;
  }

  protected final VkQueue getGraphicsQueue() {
    return graphicsQueue;
  }

  protected QueueGraphicsFamilyIndices getFamilyIndices() {
    return familyIndices;
  }

  protected final void createQueues() {
    PointerBuffer pGraphicsQueue = MemoryStack.stackGet().pointers(VK_NULL_HANDLE);

    QueueGraphicsFamilyIndices indices = getFamilyIndices();

    for (int index : indices.unique()) {
      vkGetDeviceQueue(logicalDevice, index, 0, pGraphicsQueue);
      setQueue(new VkQueue(pGraphicsQueue.get(0), logicalDevice), index, indices);
    }
  }

  protected void setQueue(VkQueue queue, int index, QueueGraphicsFamilyIndices indices) {
    if (index == indices.getGraphicsFamily()) {
      graphicsQueue = queue;
    }
  }

  protected void createLogicalDevice() {
    logInfo(" > Ch05:createLogicalDevice");

    familyIndices = findQueueFamilies(getPhysicalDevice());
    Integer graphicsFamily = familyIndices.getGraphicsFamily();

    try (MemoryStack stack = stackPush()) {
      VkDeviceQueueCreateInfo.Buffer queueCreateInfos = VkDeviceQueueCreateInfo.calloc(1, stack);

      queueCreateInfos.sType(VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO);
      queueCreateInfos.queueFamilyIndex(graphicsFamily);
      queueCreateInfos.pQueuePriorities(stack.floats(1.0f));

      VkPhysicalDeviceFeatures deviceFeatures = VkPhysicalDeviceFeatures.calloc(stack);

      VkDeviceCreateInfo createInfo = VkDeviceCreateInfo.calloc(stack);

      createInfo.sType(VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO);
      createInfo.pQueueCreateInfos(queueCreateInfos);
      // queueCreateInfoCount is automatically set

      createInfo.pEnabledFeatures(deviceFeatures);

      if (isValidationLayersRequired()) {
        // deprecated, see
        // https://www.khronos.org/registry/vulkan/specs/1.1-extensions/html/vkspec.html#extendingvulkan-layers-devicelayerdeprecation
        createInfo.ppEnabledLayerNames(asPointerBuffer(getValidationLayers()));
      }

      PointerBuffer pDevice = stack.pointers(VK_NULL_HANDLE);

      if (vkCreateDevice(getPhysicalDevice(), createInfo, null, pDevice) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create logical device");
      }

      setLogicalDevice(new VkDevice(pDevice.get(0), getPhysicalDevice(), createInfo));

      createQueues();
    }
  }
}
