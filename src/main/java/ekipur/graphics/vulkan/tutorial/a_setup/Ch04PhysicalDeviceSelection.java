package ekipur.graphics.vulkan.tutorial.a_setup;

import java.nio.IntBuffer;
import java.util.stream.IntStream;

import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch04PhysicalDeviceSelection extends Ch03ValidationLayers {
  private VkPhysicalDevice physicalDevice;

  public static void main(String[] args) {
    new Ch04PhysicalDeviceSelection().run();
  }

  public Ch04PhysicalDeviceSelection() {}

  @Override
  public void initVulkan() {
    super.initVulkan();

    logDebug("=> Ch04:initVulkan");

    pickPhysicalDevice();
  }

  protected final VkPhysicalDevice getPhysicalDevice() {
    return physicalDevice;
  }

  protected void pickPhysicalDevice() {
    logInfo(" > Ch04:pickPhysicalDevice");

    try (MemoryStack stack = stackPush()) {
      IntBuffer deviceCount = stack.ints(0);

      vkEnumeratePhysicalDevices(getInstance(), deviceCount, null);

      if (deviceCount.get(0) == 0) {
        throw new RuntimeException("Failed to find GPUs with Vulkan support");
      }

      PointerBuffer ppPhysicalDevices = stack.mallocPointer(deviceCount.get(0));

      vkEnumeratePhysicalDevices(getInstance(), deviceCount, ppPhysicalDevices);

      for (int i = 0; i < ppPhysicalDevices.capacity(); i++) {
        physicalDevice = new VkPhysicalDevice(ppPhysicalDevices.get(i), getInstance());

        if (isDeviceSuitable(physicalDevice)) {
          VkPhysicalDeviceProperties deviceProperties = VkPhysicalDeviceProperties.malloc(stack);
          vkGetPhysicalDeviceProperties(physicalDevice, deviceProperties);
          logInfo("Found suitable physical device: " + deviceProperties.deviceNameString());
          return;
        }
      }

      throw new RuntimeException("Failed to find a suitable GPU");
    }
  }

  protected boolean isDeviceSuitable(VkPhysicalDevice device) {
    logInfo(" > Ch04:isDeviceSuitable");

    QueueGraphicsFamilyIndices indices = findQueueFamilies(device);

    return indices.isComplete();
  }

  protected QueueGraphicsFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
    logInfo(" > Ch04:findQueueFamilies");

    QueueGraphicsFamilyIndices indices = new QueueGraphicsFamilyIndices();

    try (MemoryStack stack = stackPush()) {
      IntBuffer queueFamilyCount = stack.ints(0);

      vkGetPhysicalDeviceQueueFamilyProperties(device, queueFamilyCount, null);

      VkQueueFamilyProperties.Buffer queueFamilyProperties =
          VkQueueFamilyProperties.malloc(queueFamilyCount.get(0), stack);

      vkGetPhysicalDeviceQueueFamilyProperties(device, queueFamilyCount, queueFamilyProperties);

      IntStream.range(0, queueFamilyProperties.capacity())
          .filter(
              index -> (queueFamilyProperties.get(index).queueFlags() & VK_QUEUE_GRAPHICS_BIT) != 0)
          .findFirst()
          .ifPresent(index -> indices.setGraphicsFamily(index));

      return indices;
    }
  }

  public class QueueGraphicsFamilyIndices {
    // We use Integer to use null as the empty value
    private Integer graphicsFamily;

    protected boolean isComplete() {
      return getGraphicsFamily() != null;
    }

    public final Integer getGraphicsFamily() {
      return graphicsFamily;
    }

    public final void setGraphicsFamily(Integer graphicsFamily) {
      this.graphicsFamily = graphicsFamily;
    }

    public int[] unique() {
      return IntStream.of(getGraphicsFamily()).distinct().toArray();
    }
  }
}
