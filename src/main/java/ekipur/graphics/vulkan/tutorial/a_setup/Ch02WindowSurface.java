package ekipur.graphics.vulkan.tutorial.a_setup;

import java.nio.LongBuffer;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.KHRSurface;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.glfw.GLFWVulkan.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch02WindowSurface extends Ch01InstanceCreation {
  private long surface;

  public static void main(String[] args) {
    new Ch02WindowSurface().run();
  }

  public Ch02WindowSurface() {}

  @Override
  public void initVulkan() {
    super.initVulkan();

    logDebug("=> Ch02:initVulkan");

    createSurface();
  }

  @Override
  public void cleanup() {
    cleanupSurface();

    super.cleanup();
  }

  protected final long getSurface() {
    return surface;
  }

  protected final void cleanupSurface() {
    logInfo(" < Ch02:cleanupSurface");

    KHRSurface.vkDestroySurfaceKHR(getInstance(), surface, null);
  }

  protected final void createSurface() {
    logInfo(" > Ch02:createSurface");

    try (MemoryStack stack = stackPush()) {
      LongBuffer pSurface = stack.longs(VK_NULL_HANDLE);

      if (glfwCreateWindowSurface(getInstance(), getWindowHandle(), null, pSurface) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create window surface");
      }

      surface = pSurface.get(0);
    }
  }
}
