package ekipur.graphics.vulkan.tutorial.a_setup;

import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFWVulkan;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch01InstanceCreation extends Ch00BaseCode {
  private VkInstance instance;

  public static void main(String[] args) {
    new Ch01InstanceCreation().run();
  }

  public Ch01InstanceCreation() {}

  @Override
  public void initVulkan() {
    logInfo(">> Ch01:initVulkan");

    createInstance();
  }

  @Override
  public void cleanup() {
    cleanupInstance();

    super.cleanup();
  }

  protected final VkInstance getInstance() {
    return instance;
  }

  protected final void cleanupInstance() {
    logInfo(" < Ch01:cleanupInstance");

    vkDestroyInstance(instance, null);
  }

  protected boolean extendInstanceCreateInfo(VkInstanceCreateInfo createInfo) {
    logInfo(" > Ch01:extendInstanceCreateInfo (no validation layers)");

    createInfo.ppEnabledExtensionNames(GLFWVulkan.glfwGetRequiredInstanceExtensions());
    createInfo.ppEnabledLayerNames(null);
    return true;
  }

  protected final void createInstance() {
    logInfo(" > Ch01:createInstance");

    try (MemoryStack stack = stackPush()) {
      VkApplicationInfo appInfo = VkApplicationInfo.calloc(stack);

      appInfo.sType(VK_STRUCTURE_TYPE_APPLICATION_INFO);
      appInfo.pApplicationName(stack.UTF8Safe("Hallöchen Vulkan"));
      appInfo.applicationVersion(VK_MAKE_VERSION(0, 0, 1));
      appInfo.pEngineName(stack.UTF8Safe("Vulkan-Tutorial"));
      appInfo.engineVersion(VK_MAKE_VERSION(0, 0, 1));
      appInfo.apiVersion(VK_API_VERSION_1_0);

      VkInstanceCreateInfo createInfo = VkInstanceCreateInfo.calloc(stack);

      createInfo.sType(VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO);
      createInfo.pApplicationInfo(appInfo);

      if (!extendInstanceCreateInfo(createInfo)) {
        // TODO proper message
        throw new RuntimeException("Validation requested but not supported.");
      }

      PointerBuffer instancePtr = stack.mallocPointer(1);

      if (vkCreateInstance(createInfo, null, instancePtr) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create Instance.");
      }

      instance = new VkInstance(instancePtr.get(0), createInfo);
    }
  }
}
