package ekipur.graphics.vulkan.tutorial.a_setup;

import ekipur.graphics.vulkan.tutorial.TutorialApp;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.*;

public class Ch00BaseCode implements TutorialApp {
  private long windowHandle;
  private boolean stopTutorial = false;

  public static void main(String[] args) {
    new Ch00BaseCode().run();
  }

  public Ch00BaseCode() {}

  @Override
  public void initWindow() {
    logInfo(" > Ch00:initWindow");

    if (!glfwInit()) {
      throw new RuntimeException("Cannot initialize GLFW.");
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, isWindowResizable() ? GLFW_TRUE : GLFW_FALSE);

    String title = getClass().getSimpleName();

    windowHandle = glfwCreateWindow(WIDTH, HEIGHT, title, NULL, NULL);

    if (windowHandle == NULL) {
      throw new RuntimeException("Cannot create window.");
    }

    glfwSetKeyCallback(
        windowHandle,
        (window, key, scancode, action, mods) -> {
          if (key == GLFW_KEY_ESCAPE) {
            stopTutorial = (mods & GLFW_MOD_CONTROL) != 0;
            glfwSetWindowShouldClose(window, true);
          }
        });
  }

  @Override
  public void initVulkan() {}

  @Override
  public boolean mainLoop() {
    logInfo(" > Ch00:mainLoop...");

    while (!glfwWindowShouldClose(windowHandle)) {
      glfwPollEvents();
      drawFrame();
    }
    return stopTutorial;
  }

  @Override
  public void cleanup() {
    cleanupGLFW();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName();
  }

  protected final long getWindowHandle() {
    return windowHandle;
  }

  protected final void cleanupGLFW() {
    logInfo(" < Ch00:cleanupGLFW");

    glfwDestroyWindow(windowHandle);
    glfwTerminate();
  }

  protected boolean isWindowResizable() {
    return false;
  }

  protected void drawFrame() {}
}
