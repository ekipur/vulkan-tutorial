package ekipur.graphics.vulkan.tutorial.d_drawing;

import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch14CommandBuffers extends Ch13FrameBuffers {
  private long commandPool;
  private List<VkCommandBuffer> commandBuffers;

  public Ch14CommandBuffers() {}

  public static void main(String[] args) {
    new Ch14CommandBuffers().run();
  }

  @Override
  public void initVulkan() {
    super.initVulkan();

    logDebug("=> Ch14:initVulkan");

    createCommandPool();
    createCommandBuffers();
  }

  @Override
  public void cleanup() {
    cleanupCommandPool();

    super.cleanup();
  }

  protected final long getCommandPool() {
    return commandPool;
  }

  protected final List<VkCommandBuffer> getCommandBuffers() {
    return commandBuffers;
  }

  protected final void cleanupCommandPool() {
    logInfo(" < Ch14:cleanupCommandPool");

    vkDestroyCommandPool(getLogicalDevice(), commandPool, null);
  }

  protected void extendCreateCommandPool(
      VkCommandPoolCreateInfo poolInfo, LongBuffer pCommandPool) {}

  protected final void createCommandPool() {
    logInfo(" > Ch14:createCommandPool");

    try (MemoryStack stack = stackPush()) {
      QueuePresentFamilyIndices queueFamilyIndices = findQueueFamilies(getPhysicalDevice());

      VkCommandPoolCreateInfo poolInfo =
          VkCommandPoolCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO)
              .queueFamilyIndex(queueFamilyIndices.getGraphicsFamily());

      LongBuffer pCommandPool = stack.mallocLong(1);

      if (vkCreateCommandPool(getLogicalDevice(), poolInfo, null, pCommandPool) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create command pool");
      }

      commandPool = pCommandPool.get(0);

      extendCreateCommandPool(poolInfo, pCommandPool);
    }
  }

  protected VkClearValue.Buffer onRenderPassInfoClearValues(MemoryStack stack) {
    logInfo(" > Ch14:onRenderPassInfoClearValues");

    VkClearValue.Buffer clearValues = VkClearValue.calloc(1, stack);
    clearValues.color().float32(stack.floats(0.0f, 0.0f, 0.0f, 1.0f));
    return clearValues;
  }

  protected void createCommandBuffers() {
    logInfo(" > Ch14:createCommandBuffers");

    final int commandBuffersCount = getSwapChainFramebuffers().size();

    commandBuffers = new ArrayList<>(commandBuffersCount);

    try (MemoryStack stack = stackPush()) {
      VkCommandBufferAllocateInfo allocInfo =
          VkCommandBufferAllocateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
              .commandPool(commandPool)
              .level(VK_COMMAND_BUFFER_LEVEL_PRIMARY)
              .commandBufferCount(commandBuffersCount);

      PointerBuffer pCommandBuffers = stack.mallocPointer(commandBuffersCount);

      if (vkAllocateCommandBuffers(getLogicalDevice(), allocInfo, pCommandBuffers) != VK_SUCCESS) {
        throw new RuntimeException("Fialed to allocate command buffers");
      }

      for (int i = 0; i < commandBuffersCount; i++) {
        commandBuffers.add(new VkCommandBuffer(pCommandBuffers.get(i), getLogicalDevice()));
      }

      VkCommandBufferBeginInfo beginInfo =
          VkCommandBufferBeginInfo.calloc(stack).sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO);

      VkRect2D renderArea =
          VkRect2D.calloc(stack)
              .offset(VkOffset2D.calloc(stack).set(0, 0))
              .extent(getSwapChainExtent());

      VkRenderPassBeginInfo renderPassInfo =
          VkRenderPassBeginInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO)
              .renderPass(getRenderPass())
              .renderArea(renderArea)
              .pClearValues(onRenderPassInfoClearValues(stack));

      for (int i = 0; i < commandBuffersCount; i++) {
        VkCommandBuffer commandBuffer = commandBuffers.get(i);

        if (vkBeginCommandBuffer(commandBuffer, beginInfo) != VK_SUCCESS) {
          throw new RuntimeException("Failed to begin recording command buffer");
        }

        renderPassInfo.framebuffer(getSwapChainFramebuffers().get(i));

        vkCmdBeginRenderPass(commandBuffer, renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
        {
          vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, getGraphicsPipeline());

          onCmdDraw(commandBuffer, i);
        }
        vkCmdEndRenderPass(commandBuffer);

        if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS) {
          throw new RuntimeException("Failed to record command buffer");
        }
      }
    }
  }

  protected void onCmdDraw(VkCommandBuffer commandBuffer, int commandBufferIndex) {
    logInfo(" > Ch14:onCmdDraw");

    vkCmdDraw(commandBuffer, 3, 1, 0, 0);
  }
}
