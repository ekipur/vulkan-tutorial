package ekipur.graphics.vulkan.tutorial.d_drawing;

import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.*;

import ekipur.graphics.vulkan.tutorial.naitsirc98.Frame;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.KHRSwapchain.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch15HelloTriangle extends Ch14CommandBuffers {
  protected static final long UINT64_MAX = 0xFFFFFFFFFFFFFFFFL;
  private static final int MAX_FRAMES_IN_FLIGHT = 2;

  private List<Frame> inFlightFrames;
  private Map<Integer, Frame> imagesInFlight;
  private int currentFrame;

  public static void main(String[] args) {
    new Ch15HelloTriangle().run();
  }

  public Ch15HelloTriangle() {}

  @Override
  public void initVulkan() {
    super.initVulkan();

    logDebug("=> Ch15:initVulkan");

    createSyncObjects();
  }

  @Override
  public boolean mainLoop() {
    boolean stopTutorial = super.mainLoop();

    logDebug("=> Ch15:mainLoop");

    // Wait for the device to complete all operations before release resources
    vkDeviceWaitIdle(getLogicalDevice());

    return stopTutorial;
  }

  @Override
  public void cleanup() {
    cleanupSyncObjects();

    super.cleanup();
  }

  @Override
  protected final void extendRenderPassInfo(
      VkRenderPassCreateInfo renderPassInfo, MemoryStack stack) {
    logInfo(">> Ch15:extendRenderPassInfo (subpass dependency)");

    VkSubpassDependency.Buffer dependency =
        VkSubpassDependency.calloc(1, stack)
            .srcSubpass(VK_SUBPASS_EXTERNAL)
            .dstSubpass(0)
            .srcStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
            .srcAccessMask(0)
            .dstStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
            .dstAccessMask(
                VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

    renderPassInfo.pDependencies(dependency);
  }

  @Override
  protected final void drawFrame() {
    try (MemoryStack stack = stackPush()) {
      Frame thisFrame = getCurrentFrame();

      vkWaitForFences(getLogicalDevice(), thisFrame.pFence(), true, UINT64_MAX);

      IntBuffer pImageIndex = stack.mallocInt(1);

      if (!onAcquireNextImageKHR(
          vkAcquireNextImageKHR(
              getLogicalDevice(),
              getSwapChain(),
              UINT64_MAX,
              thisFrame.imageAvailableSemaphore(),
              VK_NULL_HANDLE,
              pImageIndex))) {
        return;
      }

      final int imageIndex = pImageIndex.get(0);

      onDrawFrameUpdate(imageIndex);

      if (imagesInFlight.containsKey(imageIndex)) {
        vkWaitForFences(
            getLogicalDevice(), imagesInFlight.get(imageIndex).fence(), true, UINT64_MAX);
      }

      imagesInFlight.put(imageIndex, thisFrame);

      VkSubmitInfo submitInfo =
          VkSubmitInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_SUBMIT_INFO)
              .waitSemaphoreCount(1)
              .pWaitSemaphores(thisFrame.pImageAvailableSemaphore())
              .pWaitDstStageMask(stack.ints(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT))
              .pSignalSemaphores(thisFrame.pRenderFinishedSemaphore())
              .pCommandBuffers(stack.pointers(getCommandBuffers().get(imageIndex)));

      resetFences(thisFrame.pFence());

      int vkResult = vkQueueSubmit(getGraphicsQueue(), submitInfo, thisFrame.fence());

      if (!onQueueSubmit(vkResult)) {
        throw new RuntimeException("Failed to submit draw command buffer: " + vkResult);
      }

      VkPresentInfoKHR presentInfo =
          VkPresentInfoKHR.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_PRESENT_INFO_KHR)
              .pWaitSemaphores(thisFrame.pRenderFinishedSemaphore())
              .swapchainCount(1)
              .pSwapchains(stack.longs(getSwapChain()))
              .pImageIndices(pImageIndex);

      onQueuePresentKHR(vkQueuePresentKHR(getPresentQueue(), presentInfo));

      currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }
  }

  protected final void cleanupSyncObjects() {
    logInfo(" < Ch15:cleanupSyncObjects");

    VkDevice logicalDevice = getLogicalDevice();

    inFlightFrames.forEach(
        frame -> {
          vkDestroySemaphore(logicalDevice, frame.renderFinishedSemaphore(), null);
          vkDestroySemaphore(logicalDevice, frame.imageAvailableSemaphore(), null);
          vkDestroyFence(logicalDevice, frame.fence(), null);
        });
    imagesInFlight.clear();
  }

  protected final void createSyncObjects() {
    logInfo(" > Ch15:createSyncObjects");

    inFlightFrames = new ArrayList<>(MAX_FRAMES_IN_FLIGHT);
    imagesInFlight = new HashMap<>(getSwapChainImages().size());

    try (MemoryStack stack = stackPush()) {
      VkSemaphoreCreateInfo semaphoreInfo =
          VkSemaphoreCreateInfo.calloc(stack).sType(VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO);

      VkFenceCreateInfo fenceInfo =
          VkFenceCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_FENCE_CREATE_INFO)
              .flags(VK_FENCE_CREATE_SIGNALED_BIT);

      LongBuffer pImageAvailableSemaphore = stack.mallocLong(1);
      LongBuffer pRenderFinishedSemaphore = stack.mallocLong(1);
      LongBuffer pFence = stack.mallocLong(1);

      for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (vkCreateSemaphore(getLogicalDevice(), semaphoreInfo, null, pImageAvailableSemaphore)
                != VK_SUCCESS
            || vkCreateSemaphore(getLogicalDevice(), semaphoreInfo, null, pRenderFinishedSemaphore)
                != VK_SUCCESS
            || vkCreateFence(getLogicalDevice(), fenceInfo, null, pFence) != VK_SUCCESS) {

          throw new RuntimeException("Failed to create synchronization objects for the frame " + i);
        }

        inFlightFrames.add(
            new Frame(
                pImageAvailableSemaphore.get(0), pRenderFinishedSemaphore.get(0), pFence.get(0)));
      }
    }
  }

  protected final Frame getCurrentFrame() {
    return inFlightFrames.get(currentFrame);
  }

  protected final void resetFences(LongBuffer pFences) {
    vkResetFences(getLogicalDevice(), pFences);
  }

  protected boolean onAcquireNextImageKHR(int vkResult) {
    return true;
  }

  protected void onQueuePresentKHR(int vkResult) {}

  protected boolean onQueueSubmit(int vkResult) {
    return vkResult == VK_SUCCESS;
  }

  protected void onDrawFrameUpdate(int imageIndex) {}
}
