package ekipur.graphics.vulkan.tutorial.d_drawing;

import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.List;

import ekipur.graphics.vulkan.tutorial.c_graphicspipelinebasics.Ch12GraphicsPipelineComplete;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkFramebufferCreateInfo;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch13FrameBuffers extends Ch12GraphicsPipelineComplete {
  private List<Long> swapChainFramebuffers;

  public static void main(String[] args) {
    new Ch13FrameBuffers().run();
  }

  public Ch13FrameBuffers() {}

  @Override
  public void initVulkan() {
    super.initVulkan();

    logDebug("=> Ch13:initVulkan");

    createFramebuffers();
  }

  @Override
  public void cleanup() {
    cleanupFramebuffers();

    super.cleanup();
  }

  protected final List<Long> getSwapChainFramebuffers() {
    return swapChainFramebuffers;
  }

  protected final void cleanupFramebuffers() {
    logInfo(" < Ch13:cleanupFramebuffers");

    swapChainFramebuffers.forEach(
        framebuffer -> vkDestroyFramebuffer(getLogicalDevice(), framebuffer, null));
  }

  protected LongBuffer allocateFramebufferAttachments(MemoryStack stack) {
    return stack.mallocLong(1);
  }

  protected void putSwapChainImageViewToFramebufferAttachments(
      LongBuffer attachments, long imageView) {
    attachments.put(0, imageView);
  }

  protected void createFramebuffers() {
    logInfo(" > Ch13:createFramebuffers");

    swapChainFramebuffers = new ArrayList<>(getSwapChainImageViews().size());

    try (MemoryStack stack = stackPush()) {
      LongBuffer attachments = allocateFramebufferAttachments(stack);
      LongBuffer pFramebuffer = stack.mallocLong(1);

      // Lets allocate the create info struct once and just update the
      // pAttachments field each iteration
      VkFramebufferCreateInfo framebufferInfo =
          VkFramebufferCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO)
              .renderPass(getRenderPass())
              .width(getSwapChainExtent().width())
              .height(getSwapChainExtent().height())
              .layers(1);

      for (long imageView : getSwapChainImageViews()) {
        putSwapChainImageViewToFramebufferAttachments(attachments, imageView);
        framebufferInfo.pAttachments(attachments);

        if (vkCreateFramebuffer(getLogicalDevice(), framebufferInfo, null, pFramebuffer)
            != VK_SUCCESS) {
          throw new RuntimeException("Failed to create framebuffer");
        }

        swapChainFramebuffers.add(pFramebuffer.get(0));
      }
    }
  }
}
