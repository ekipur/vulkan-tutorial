package ekipur.graphics.vulkan.tutorial.h_texturemapping;

import java.nio.LongBuffer;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch24Sampler extends Ch23TextureImage {
  private long textureImageView;
  private long textureSampler;

  public static void main(String[] args) {
    new Ch24Sampler().run();
  }

  public Ch24Sampler() {}

  @Override
  protected final void createTextureImage() {
    super.createTextureImage();

    logDebug("=> Ch23:createTextureImage");

    createTextureImageView();
    createTextureSampler();
  }

  @Override
  protected final void cleanupTextureImage() {
    logDebug("=< Ch23:cleanupTextureImage");

    cleanupTextureSamplerAndImageView();

    super.cleanupTextureImage();
  }

  @Override
  protected boolean checkAnisotropySupported() {
    logInfo(">> Ch24:checkAnisotropySupported");

    boolean anisotrop = false;

    try (MemoryStack stack = stackPush()) {
      VkPhysicalDeviceFeatures supportedFeatures = VkPhysicalDeviceFeatures.malloc(stack);
      vkGetPhysicalDeviceFeatures(getPhysicalDevice(), supportedFeatures);
      anisotrop = supportedFeatures.samplerAnisotropy();
    }
    return anisotrop;
  }

  @Override
  protected void setImageViewComponents(VkImageViewCreateInfo info) {
    logInfo(">> Ch24:setImageViewComponents (no more setting of default component swizzles)");
  }

  @Override
  protected void extendDeviceFeatures(VkPhysicalDeviceFeatures deviceFeatures) {
    deviceFeatures.samplerAnisotropy(true);
  }

  protected final long getTextureImageView() {
    return textureImageView;
  }

  protected final long getTextureSampler() {
    return textureSampler;
  }

  private void createTextureImageView() {
    logInfo(" > Ch24:createTextureImageView");

    textureImageView =
        createImageView(getTextureImage(), VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT);
  }

  private void cleanupTextureSamplerAndImageView() {
    logInfo(" < Ch24:cleanupTextureSamplerAndImageView");

    vkDestroySampler(getLogicalDevice(), getTextureSampler(), null);
    vkDestroyImageView(getLogicalDevice(), getTextureImageView(), null);
  }

  protected void extendTextureSamplerCreateInfo(VkSamplerCreateInfo samplerInfo) {}

  protected void createTextureSampler() {
    logInfo(" > Ch24:createTextureSampler");

    try (MemoryStack stack = stackPush()) {
      VkSamplerCreateInfo samplerInfo =
          VkSamplerCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO)
              .magFilter(VK_FILTER_LINEAR)
              .minFilter(VK_FILTER_LINEAR)
              .addressModeU(VK_SAMPLER_ADDRESS_MODE_REPEAT)
              .addressModeV(VK_SAMPLER_ADDRESS_MODE_REPEAT)
              .addressModeW(VK_SAMPLER_ADDRESS_MODE_REPEAT)
              .anisotropyEnable(checkAnisotropySupported())
              .maxAnisotropy(16.0f)
              .borderColor(VK_BORDER_COLOR_INT_OPAQUE_BLACK)
              .unnormalizedCoordinates(false)
              .compareEnable(false)
              .compareOp(VK_COMPARE_OP_ALWAYS)
              .mipmapMode(VK_SAMPLER_MIPMAP_MODE_LINEAR);

      extendTextureSamplerCreateInfo(samplerInfo);

      LongBuffer pTextureSampler = stack.mallocLong(1);

      if (vkCreateSampler(getLogicalDevice(), samplerInfo, null, pTextureSampler) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create texture sampler");
      }

      textureSampler = pTextureSampler.get(0);
    }
  }
}
