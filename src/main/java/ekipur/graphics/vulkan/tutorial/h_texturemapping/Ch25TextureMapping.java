package ekipur.graphics.vulkan.tutorial.h_texturemapping;

import java.util.ArrayList;

import ekipur.graphics.vulkan.tutorial.naitsirc98.ShaderSPIRVUtils.ShaderKind;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch25TextureMapping extends Ch24Sampler {
  private final VertexInfo<?> vertexInfo;

  public static void main(String[] args) {
    new Ch25TextureMapping().run();
  }

  public Ch25TextureMapping() {
    vertexInfo =
        new VertexInfo<>(
            new ArrayList<>() {
              {
                add(
                    new VertexExt<>(
                        new Vector2f(-0.5f, -0.5f),
                        new Vector3f(1.0f, 0.0f, 0.0f),
                        new Vector2f(0.0f, 0.0f)));
                add(
                    new VertexExt<>(
                        new Vector2f(0.5f, -0.5f),
                        new Vector3f(0.0f, 1.0f, 0.0f),
                        new Vector2f(1.0f, 0.0f)));
                add(
                    new VertexExt<>(
                        new Vector2f(0.5f, 0.5f),
                        new Vector3f(0.0f, 0.0f, 1.0f),
                        new Vector2f(1.0f, 1.0f)));
                add(
                    new VertexExt<>(
                        new Vector2f(-0.5f, 0.5f),
                        new Vector3f(1.0f, 1.0f, 1.0f),
                        new Vector2f(0.0f, 1.0f)));
              }
            });
  }

  @Override
  protected VertexInfo<?> getVertexInfo() {
    return vertexInfo;
  }

  @Override
  protected void extendGraphicsPipline(
      MemoryStack stack, GraphicsPipelineElements pipelineElements) {
    if (pipelineElements.shaderSources.isEmpty()) {
      logInfo("=> Ch25:extendGraphicsPipeline (25_shader_textures)");

      pipelineElements.shaderSources.put(
          ShaderKind.VERTEX_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/25_shader_textures.vert");

      pipelineElements.shaderSources.put(
          ShaderKind.FRAGMENT_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/25_shader_textures.frag");
    }

    super.extendGraphicsPipline(stack, pipelineElements);
  }

  @Override
  protected int getDescriptorPoolSizeCount() {
    return super.getDescriptorPoolSizeCount() + 1;
  }

  @Override
  protected boolean updateDescriptorPoolSize(int index, VkDescriptorPoolSize poolSize) {
    boolean updated = super.updateDescriptorPoolSize(index, poolSize);

    if (!updated && index == 1) {
      logInfo("-> Ch25:updateDescriptorPoolSize " + index + " (combined image sampler)");

      poolSize
          .type(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER)
          .descriptorCount(getSwapChainImages().size());

      return true;
    }
    return updated;
  }

  @Override
  protected int getDescriptorSetLayoutBindingCount() {
    return super.getDescriptorSetLayoutBindingCount() + 1;
  }

  @Override
  protected boolean setDescriptorSetLayoutBinding(int index, VkDescriptorSetLayoutBinding binding) {
    boolean bound = super.setDescriptorSetLayoutBinding(index, binding);

    if (!bound && index == 1) {
      logInfo("~> Ch25:setDescriptorSetLayoutBinding " + index);

      binding
          .binding(index)
          .descriptorCount(1)
          .descriptorType(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER)
          .pImmutableSamplers(null)
          .stageFlags(VK_SHADER_STAGE_FRAGMENT_BIT);

      return true;
    }
    return bound;
  }

  @Override
  protected int getDescriptorWriteCount() {
    return super.getDescriptorWriteCount() + 1;
  }

  @Override
  protected boolean fillWriteDescriptorSet(
      int index, VkWriteDescriptorSet descriptorWrite, MemoryStack stack) {
    boolean updated = super.fillWriteDescriptorSet(index, descriptorWrite, stack);

    if (!updated && index == 1) {
      logInfo("-> Ch25:fillWriteDescriptorSet " + index + " (combined image sampler)");

      VkDescriptorImageInfo.Buffer imageInfo =
          VkDescriptorImageInfo.calloc(1, stack)
              .imageLayout(VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
              .imageView(getTextureImageView())
              .sampler(getTextureSampler());

      descriptorWrite
          .sType(VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET)
          .dstBinding(1)
          .dstArrayElement(0)
          .descriptorType(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER)
          .descriptorCount(1)
          .pImageInfo(imageInfo);

      return true;
    }
    return updated;
  }
}
