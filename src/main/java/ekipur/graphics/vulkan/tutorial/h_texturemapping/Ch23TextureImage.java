package ekipur.graphics.vulkan.tutorial.h_texturemapping;

import java.nio.*;
import java.util.function.BiConsumer;

import ekipur.graphics.vulkan.tutorial.g_uniformbuffers.Ch22DescriptorSets;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.stb.STBImage.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch23TextureImage extends Ch22DescriptorSets {
  private long textureImage;
  private long textureImageMemory;

  public static void main(String[] args) {
    new Ch23TextureImage().run();
  }

  public Ch23TextureImage() {}

  @Override
  protected void createVertexBuffer() {
    logDebug("=> Ch23:createVertexBuffer");

    createTextureImage();

    super.createVertexBuffer();
  }

  @Override
  protected void copyBuffer(long srcBuffer, long dstBuffer, long size) {
    logInfo(">> Ch23:copyBuffer");

    try (MemoryStack stack = stackPush()) {
      VkCommandBuffer commandBuffer = beginSingleTimeCommands();

      VkBufferCopy.Buffer copyRegion = VkBufferCopy.calloc(1, stack).size(size);

      vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, copyRegion);

      endSingleTimeCommands(commandBuffer);
    }
  }

  @Override
  protected void cleanupDescriptorSetLayout() {
    logDebug("=< Ch21:cleanupDescriptorSetLayout");

    cleanupTextureImage();

    super.cleanupDescriptorSetLayout();
  }

  protected final long getTextureImage() {
    return textureImage;
  }

  protected void cleanupTextureImage() {
    logInfo(" < Ch23:cleanupTextureImage");

    vkDestroyImage(getLogicalDevice(), getTextureImage(), null);
    vkFreeMemory(getLogicalDevice(), textureImageMemory, null);
  }

  protected String getImageFilename() {
    return "texture.jpg";
  }

  protected void createTextureImage() {
    logInfo(" > Ch23:createTextureImage");

    try (MemoryStack stack = stackPush()) {
      String filename =
          getClass()
              .getClassLoader()
              .getResource(
                  "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/textures/"
                      + getImageFilename())
              .getPath();

      IntBuffer pWidth = stack.mallocInt(1);
      IntBuffer pHeight = stack.mallocInt(1);
      IntBuffer pChannels = stack.mallocInt(1);

      ByteBuffer pixels = stbi_load(filename, pWidth, pHeight, pChannels, STBI_rgb_alpha);

      if (pixels == null) {
        throw new RuntimeException("Failed to load texture image " + filename);
      }

      int imageWidth = pWidth.get(0);
      int imageHeight = pHeight.get(0);
      // imageSize correctly calculated with 4 channels instead of
      // pChannels.get(0) since pChannels.get(0) contains the actual number of
      // channels which could be less then 4, e.g. due to a missing alpha
      // channel.
      long imageSize = imageWidth * imageHeight * 4;

      logInfo(
          " > Image loaded: ["
              + imageWidth
              + " x "
              + imageHeight
              + " x "
              + pChannels.get(0)
              + "] "
              + filename);

      LongBuffer pStagingBuffer = stack.mallocLong(1);
      LongBuffer pStagingBufferMemory = stack.mallocLong(1);

      createBuffer(
          imageSize,
          VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
          pStagingBuffer,
          pStagingBufferMemory);

      PointerBuffer data = stack.mallocPointer(1);
      vkMapMemory(getLogicalDevice(), pStagingBufferMemory.get(0), 0, imageSize, 0, data);
      {
        memcpy(data.getByteBuffer(0, (int) imageSize), pixels, imageSize);
      }
      vkUnmapMemory(getLogicalDevice(), pStagingBufferMemory.get(0));

      stbi_image_free(pixels);

      LongBuffer pTextureImage = stack.mallocLong(1);
      LongBuffer pTextureImageMemory = stack.mallocLong(1);

      createImage(
          pWidth.get(0),
          pHeight.get(0),
          VK_SAMPLE_COUNT_1_BIT,
          VK_FORMAT_R8G8B8A8_SRGB,
          VK_IMAGE_TILING_OPTIMAL,
          getImageInfoUsage(),
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
          pTextureImage,
          pTextureImageMemory);

      textureImage = pTextureImage.get(0);
      textureImageMemory = pTextureImageMemory.get(0);

      transitionImageLayout(
          getTextureImage(),
          VK_FORMAT_R8G8B8A8_SRGB,
          VK_IMAGE_LAYOUT_UNDEFINED,
          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

      copyBufferToImage(pStagingBuffer.get(0), getTextureImage(), pWidth.get(0), pHeight.get(0));

      onTransitionTextureImageLayout(
          getTextureImage(), VK_FORMAT_R8G8B8A8_SRGB, pWidth.get(0), pHeight.get(0));

      cleanupBuffer(pStagingBuffer.get(0), pStagingBufferMemory.get(0));
    }
  }

  protected int getImageInfoUsage() {
    return VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
  }

  protected void onTransitionTextureImageLayout(
      long image, int imageFormat, int width, int height) {
    logInfo(" > Ch23:onTransitionTextureImageLayout");

    transitionImageLayout(
        image,
        imageFormat,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
  }

  protected final void createImage(
      int width,
      int height,
      int numSamples,
      int format,
      int tiling,
      int usage,
      int memProperties,
      LongBuffer pTextureImage,
      LongBuffer pTextureImageMemory) {
    try (MemoryStack stack = stackPush()) {
      VkImageCreateInfo imageInfo =
          VkImageCreateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO)
              .imageType(VK_IMAGE_TYPE_2D)
              .mipLevels(getMipLevels())
              .arrayLayers(1)
              .format(format)
              .tiling(tiling)
              .initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
              .usage(usage)
              .samples(numSamples)
              .sharingMode(VK_SHARING_MODE_EXCLUSIVE);
      imageInfo.extent().width(width).height(height).depth(1);

      if (vkCreateImage(getLogicalDevice(), imageInfo, null, pTextureImage) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create image");
      }

      VkMemoryRequirements memRequirements = VkMemoryRequirements.malloc(stack);
      vkGetImageMemoryRequirements(getLogicalDevice(), pTextureImage.get(0), memRequirements);

      VkMemoryAllocateInfo allocInfo =
          VkMemoryAllocateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
              .allocationSize(memRequirements.size())
              .memoryTypeIndex(findMemoryType(memRequirements.memoryTypeBits(), memProperties));

      if (vkAllocateMemory(getLogicalDevice(), allocInfo, null, pTextureImageMemory)
          != VK_SUCCESS) {
        throw new RuntimeException("Failed to allocate image memory");
      }

      vkBindImageMemory(getLogicalDevice(), pTextureImage.get(0), pTextureImageMemory.get(0), 0);
    }
  }

  protected void onImageMemoryBarrierPrecondition(
      VkImageMemoryBarrier.Buffer barrier, int format, int oldLayout, int newLayout) {
    logInfo(" > Ch23:onImageMemoryBarrierPrecondition");

    barrier.subresourceRange().aspectMask(VK_IMAGE_ASPECT_COLOR_BIT);
  }

  protected boolean onImageMemoryBarrierPostcondition(
      VkImageMemoryBarrier.Buffer barrier,
      int oldLayout,
      int newLayout,
      BiConsumer<Integer, Integer> stageMaskConsumer) {
    boolean consumed = false;

    if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED
        && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
      barrier.srcAccessMask(0).dstAccessMask(VK_ACCESS_TRANSFER_WRITE_BIT);

      stageMaskConsumer.accept(VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT);
      consumed = true;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
      barrier.srcAccessMask(VK_ACCESS_TRANSFER_WRITE_BIT).dstAccessMask(VK_ACCESS_SHADER_READ_BIT);

      stageMaskConsumer.accept(
          VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
      consumed = true;
    }

    if (consumed) {
      logInfo(" > Ch23:onImageMemoryBarrierPostcondition");
    }
    return consumed;
  }

  protected final void transitionImageLayout(long image, int format, int oldLayout, int newLayout) {
    try (MemoryStack stack = stackPush()) {
      VkImageMemoryBarrier.Buffer barrier =
          VkImageMemoryBarrier.calloc(1, stack)
              .sType(VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER)
              .oldLayout(oldLayout)
              .newLayout(newLayout)
              .srcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
              .dstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
              .image(image);
      barrier
          .subresourceRange()
          .baseMipLevel(0)
          .levelCount(getMipLevels())
          .baseArrayLayer(0)
          .layerCount(1);

      onImageMemoryBarrierPrecondition(barrier, format, oldLayout, newLayout);

      if (!onImageMemoryBarrierPostcondition(
          barrier,
          oldLayout,
          newLayout,
          (srcStageMask, dstStageMask) -> {
            VkCommandBuffer commandBuffer = beginSingleTimeCommands();

            vkCmdPipelineBarrier(commandBuffer, srcStageMask, dstStageMask, 0, null, null, barrier);

            endSingleTimeCommands(commandBuffer);
          })) {
        throw new IllegalArgumentException("Unsupported layout transition");
      }
    }
  }

  private void copyBufferToImage(long buffer, long image, int width, int height) {
    try (MemoryStack stack = stackPush()) {
      VkCommandBuffer commandBuffer = beginSingleTimeCommands();

      VkBufferImageCopy.Buffer region =
          VkBufferImageCopy.calloc(1, stack)
              .bufferOffset(0)
              .bufferRowLength(0) // Tightly packed
              .bufferImageHeight(0); // Tightly packed
      region.imageOffset().set(0, 0, 0);
      region.imageExtent(VkExtent3D.calloc(stack).set(width, height, 1));
      region
          .imageSubresource()
          .aspectMask(VK_IMAGE_ASPECT_COLOR_BIT)
          .mipLevel(0)
          .baseArrayLayer(0)
          .layerCount(1);

      vkCmdCopyBufferToImage(
          commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, region);

      endSingleTimeCommands(commandBuffer);
    }
  }

  protected final VkCommandBuffer beginSingleTimeCommands() {
    try (MemoryStack stack = stackPush()) {
      VkCommandBufferAllocateInfo allocInfo =
          VkCommandBufferAllocateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
              .level(VK_COMMAND_BUFFER_LEVEL_PRIMARY)
              .commandPool(getCommandPool())
              .commandBufferCount(1);

      PointerBuffer pCommandBuffer = stack.mallocPointer(1);
      vkAllocateCommandBuffers(getLogicalDevice(), allocInfo, pCommandBuffer);
      VkCommandBuffer commandBuffer =
          new VkCommandBuffer(pCommandBuffer.get(0), getLogicalDevice());

      VkCommandBufferBeginInfo beginInfo =
          VkCommandBufferBeginInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
              .flags(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

      vkBeginCommandBuffer(commandBuffer, beginInfo);

      return commandBuffer;
    }
  }

  protected final void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
    try (MemoryStack stack = stackPush()) {
      vkEndCommandBuffer(commandBuffer);

      VkSubmitInfo.Buffer submitInfo =
          VkSubmitInfo.calloc(1, stack)
              .sType(VK_STRUCTURE_TYPE_SUBMIT_INFO)
              .pCommandBuffers(stack.pointers(commandBuffer));

      vkQueueSubmit(getGraphicsQueue(), submitInfo, VK_NULL_HANDLE);
      vkQueueWaitIdle(getGraphicsQueue());

      vkFreeCommandBuffers(getLogicalDevice(), getCommandPool(), commandBuffer);
    }
  }

  private void memcpy(ByteBuffer dst, ByteBuffer src, long size) {
    src.limit((int) size);
    dst.put(src);
    src.limit(src.capacity()).rewind();
  }
}
