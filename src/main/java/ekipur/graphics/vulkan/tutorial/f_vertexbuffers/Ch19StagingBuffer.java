package ekipur.graphics.vulkan.tutorial.f_vertexbuffers;

import java.nio.LongBuffer;

import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch19StagingBuffer extends Ch18VertexBuffer {
  public static void main(String[] args) {
    new Ch19StagingBuffer().run();
  }

  public Ch19StagingBuffer() {}

  @Override
  protected void createVertexBuffer() {
    logInfo(">> Ch19:createVertexBuffer");

    try (MemoryStack stack = stackPush()) {
      long bufferSize = getVertexInfo().getVertexSize() * getVertexInfo().getVertices().size();

      LongBuffer pVertexBuffer = stack.mallocLong(1);
      LongBuffer pVertexBufferMemory = stack.mallocLong(1);

      createBuffer(
          bufferSize,
          VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
          pVertexBuffer,
          pVertexBufferMemory);

      long stagingBuffer = pVertexBuffer.get(0);
      long stagingBufferMemory = pVertexBufferMemory.get(0);

      PointerBuffer data = stack.mallocPointer(1);

      vkMapMemory(getLogicalDevice(), stagingBufferMemory, 0, bufferSize, 0, data);
      {
        getVertexInfo().memCopy(data.getByteBuffer(0, (int) bufferSize));
      }
      vkUnmapMemory(getLogicalDevice(), stagingBufferMemory);

      createBuffer(
          bufferSize,
          VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
          VK_MEMORY_HEAP_DEVICE_LOCAL_BIT,
          pVertexBuffer,
          pVertexBufferMemory);

      setVertexBuffer(pVertexBuffer.get(0));
      setVertexBufferMemory(pVertexBufferMemory.get(0));

      copyBuffer(stagingBuffer, getVertexBuffer(), bufferSize);

      cleanupBuffer(stagingBuffer, stagingBufferMemory);
    }
  }

  protected void copyBuffer(long srcBuffer, long dstBuffer, long size) {
    logInfo(" > Ch19:copyBuffer");

    MemoryStack stack = stackGet();

    VkCommandBufferAllocateInfo allocInfo =
        VkCommandBufferAllocateInfo.calloc(stack)
            .sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
            .level(VK_COMMAND_BUFFER_LEVEL_PRIMARY)
            .commandPool(getCommandPool())
            .commandBufferCount(1);

    PointerBuffer pCommandBuffer = stack.mallocPointer(1);
    vkAllocateCommandBuffers(getLogicalDevice(), allocInfo, pCommandBuffer);
    VkCommandBuffer commandBuffer = new VkCommandBuffer(pCommandBuffer.get(0), getLogicalDevice());

    VkCommandBufferBeginInfo beginInfo =
        VkCommandBufferBeginInfo.calloc(stack)
            .sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
            .flags(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

    vkBeginCommandBuffer(commandBuffer, beginInfo);
    {
      VkBufferCopy.Buffer copyRegion = VkBufferCopy.calloc(1, stack);
      copyRegion.size(size);
      vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, copyRegion);
    }
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo =
        VkSubmitInfo.calloc(stack)
            .sType(VK_STRUCTURE_TYPE_SUBMIT_INFO)
            .pCommandBuffers(pCommandBuffer);

    if (vkQueueSubmit(getGraphicsQueue(), submitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
      throw new RuntimeException("Failed to submit copy command buffer");
    }

    vkQueueWaitIdle(getGraphicsQueue());

    vkFreeCommandBuffers(getLogicalDevice(), getCommandPool(), pCommandBuffer);
  }
}
