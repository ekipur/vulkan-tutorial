package ekipur.graphics.vulkan.tutorial.f_vertexbuffers;

import java.nio.ByteBuffer;
import java.util.List;

import ekipur.graphics.vulkan.tutorial.e_swapchainrecreation.Ch16SwapChainRecreation;
import org.joml.Vector2fc;
import org.joml.Vector3fc;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkVertexInputAttributeDescription;
import org.lwjgl.vulkan.VkVertexInputBindingDescription;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static ekipur.graphics.vulkan.tutorial.naitsirc98.ShaderSPIRVUtils.ShaderKind.*;
import static org.lwjgl.vulkan.VK10.*;

public abstract class Ch17VertexInputDescription extends Ch16SwapChainRecreation {
  public Ch17VertexInputDescription() {}

  @Override
  protected void extendGraphicsPipline(
      MemoryStack stack, GraphicsPipelineElements pipelineElements) {
    if (pipelineElements.shaderSources.isEmpty()) {
      logInfo("=> Ch17:extendGraphicsPipeline (17_shader_vertex, binding/attribute descriptions)");

      pipelineElements.shaderSources.put(
          VERTEX_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/17_shader_vertexbuffer.vert");

      pipelineElements.shaderSources.put(
          FRAGMENT_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/17_shader_vertexbuffer.frag");
    } else {
      logInfo("=> Ch17:extendGraphicsPipeline (binding/attribute descriptions)");
    }

    pipelineElements
        .vertexInputInfo
        .pVertexBindingDescriptions(getVertexInfo().getBindingDescription())
        .pVertexAttributeDescriptions(getVertexInfo().getAttributeDescriptions());

    super.extendGraphicsPipline(stack, pipelineElements);
  }

  @Override
  protected final boolean onQueueSubmit(int vkResult) {
    if (vkResult != VK_SUCCESS) {
      resetFences(getCurrentFrame().pFence());
      return false;
    }
    return true;
  }

  protected abstract VertexInfo<?> getVertexInfo();

  public class VertexInfo<V> {
    private final List<Vertex<V>> vertices;
    private final int posElementCount;

    public VertexInfo(List<Vertex<V>> vertices) {
      this.vertices = vertices;
      posElementCount = vertices.get(0).getPos() instanceof Vector3fc ? 3 : 2;
    }

    public int getOffsetPos() {
      return 0;
    }

    public int getOffsetColor() {
      return posElementCount * Float.BYTES;
    }

    public int getOffsetTexcoord() {
      return (posElementCount + 3) * Float.BYTES;
    }

    public int getAttributeCount() {
      return vertices.get(0) instanceof VertexExt<?> ? 3 : 2;
    }

    public int getVertexSize() {
      return (getAttributeCount() == 3 ? posElementCount + 3 + 2 : posElementCount + 3)
          * Float.BYTES;
    }

    public List<Vertex<V>> getVertices() {
      return vertices;
    }

    public void memCopy(ByteBuffer buffer) {
      vertices.forEach(
          vertex -> {
            if (posElementCount == 3) {
              Vector3fc pos = (Vector3fc) vertex.getPos();
              buffer.putFloat(pos.x()).putFloat(pos.y()).putFloat(pos.z());
            } else {
              Vector2fc pos = (Vector2fc) vertex.getPos();
              buffer.putFloat(pos.x()).putFloat(pos.y());
            }

            Vector3fc color = vertex.getColor();
            buffer.putFloat(color.x()).putFloat(color.y()).putFloat(color.z());

            if (getAttributeCount() == 3) {
              Vector2fc texcoord = ((VertexExt<?>) vertex).getTexcoord();
              buffer.putFloat(texcoord.x()).putFloat(texcoord.y());
            }
          });
      buffer.rewind();
    }

    public VkVertexInputBindingDescription.Buffer getBindingDescription() {
      VkVertexInputBindingDescription.Buffer bindingDescription =
          VkVertexInputBindingDescription.calloc(1)
              .binding(0)
              .stride(getVertexSize())
              .inputRate(VK_VERTEX_INPUT_RATE_VERTEX);

      return bindingDescription;
    }

    public VkVertexInputAttributeDescription.Buffer getAttributeDescriptions() {
      VkVertexInputAttributeDescription.Buffer attributeDescriptions =
          VkVertexInputAttributeDescription.calloc(getAttributeCount());

      for (int i = 0; i < getAttributeCount(); i++) {
        VkVertexInputAttributeDescription description =
            attributeDescriptions.get(i).binding(0).location(i);

        switch (i) {
          case 0:
            // Position
            description
                .format(posElementCount == 3 ? VK_FORMAT_R32G32B32_SFLOAT : VK_FORMAT_R32G32_SFLOAT)
                .offset(getOffsetPos());
            break;
          case 1:
            // Color
            description.format(VK_FORMAT_R32G32B32_SFLOAT).offset(getOffsetColor());
            break;
          case 2:
            // Texture coordinates
            description.format(VK_FORMAT_R32G32_SFLOAT).offset(getOffsetTexcoord());
            break;
          default:
            logWarn("Unexpected attribute description index: " + i);
            break;
        }
      }

      return attributeDescriptions.rewind();
    }
  }

  public class Vertex<P> {
    private final P pos;
    private final Vector3fc color;

    public Vertex(P pos, Vector3fc color) {
      this.pos = pos;
      this.color = color;
    }

    public P getPos() {
      return pos;
    }

    public Vector3fc getColor() {
      return color;
    }
  }

  public class VertexExt<P> extends Vertex<P> {
    private final Vector2fc texcoord;

    public VertexExt(P pos, Vector3fc color, Vector2fc texcoord) {
      super(pos, color);

      this.texcoord = texcoord;
    }

    public Vector2fc getTexcoord() {
      return texcoord;
    }
  }
}
