package ekipur.graphics.vulkan.tutorial.f_vertexbuffers;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkCommandBuffer;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch20IndexBuffer extends Ch19StagingBuffer {
  private final VertexInfo<?> vertexInfo;
  private final ShortIndexInfo indexInfo;
  private long indexBuffer;
  private long indexBufferMemory;

  public static void main(String[] args) {
    new Ch20IndexBuffer().run();
  }

  public Ch20IndexBuffer() {
    vertexInfo =
        new VertexInfo<>(
            new ArrayList<>() {
              {
                add(new Vertex<>(new Vector2f(-0.5f, -0.5f), new Vector3f(1.0f, 0.0f, 0.0f)));
                add(new Vertex<>(new Vector2f(0.5f, -0.5f), new Vector3f(0.0f, 1.0f, 0.0f)));
                add(new Vertex<>(new Vector2f(0.5f, 0.5f), new Vector3f(0.0f, 0.0f, 1.0f)));
                add(new Vertex<>(new Vector2f(-0.5f, 0.5f), new Vector3f(1.0f, 1.0f, 1.0f)));
              }
            });

    indexInfo =
        new ShortIndexInfo(
            new ArrayList<>() {
              {
                add((short) 2);
                add((short) 1);
                add((short) 0);
                add((short) 0);
                add((short) 3);
                add((short) 2);
              }
            });
  }

  @Override
  protected boolean isFrontFaceClockwise() {
    return false;
  }

  @Override
  protected VertexInfo<?> getVertexInfo() {
    return vertexInfo;
  }

  @Override
  protected final void onCmdDraw(VkCommandBuffer commandBuffer, int commandBufferIndex) {
    logInfo(">> Ch20:onCmdDraw");

    bindVertexBuffer(commandBuffer);
    bindIndexBuffer(commandBuffer);
    bindDescriptorSets(commandBuffer, commandBufferIndex);

    onCmdDrawIndexed(commandBuffer);
  }

  @Override
  protected void createVertexBuffer() {
    super.createVertexBuffer();

    logDebug("=> Ch20:createVertexBuffer");

    createIndexBuffer();
  }

  @Override
  protected void cleanupVertexBuffer() {
    logDebug("=< Ch20:cleanupVertexBuffer");

    cleanupIndexBuffer();

    super.cleanupVertexBuffer();
  }

  protected final long getIndexBuffer() {
    return indexBuffer;
  }

  protected IndexInfo<?> getIndexInfo() {
    return indexInfo;
  }

  protected final void onCmdDrawIndexed(VkCommandBuffer commandBuffer) {
    vkCmdDrawIndexed(commandBuffer, getIndexInfo().getIndices().size(), 1, 0, 0, 0);
  }

  protected int getIndexType() {
    return VK_INDEX_TYPE_UINT16;
  }

  protected final void bindIndexBuffer(VkCommandBuffer commandBuffer) {
    vkCmdBindIndexBuffer(commandBuffer, getIndexBuffer(), 0, getIndexType());
  }

  protected void bindDescriptorSets(VkCommandBuffer commandBuffer, int commandBufferIndex) {}

  private void cleanupIndexBuffer() {
    logInfo(" < Ch20:cleanupIndexBuffer");

    cleanupBuffer(getIndexBuffer(), indexBufferMemory);
  }

  private void createIndexBuffer() {
    logInfo(" > Ch20:createIndexBuffer");

    try (MemoryStack stack = MemoryStack.stackPush()) {
      long bufferSize = getIndexInfo().getBufferSize();

      LongBuffer pBuffer = stack.mallocLong(1);
      LongBuffer pBufferMemory = stack.mallocLong(1);

      createBuffer(
          bufferSize,
          VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
          pBuffer,
          pBufferMemory);

      long stagingBuffer = pBuffer.get(0);
      long stagingBufferMemory = pBufferMemory.get(0);

      PointerBuffer data = stack.mallocPointer(1);

      vkMapMemory(getLogicalDevice(), stagingBufferMemory, 0, bufferSize, 0, data);
      {
        getIndexInfo().memCopy(data.getByteBuffer(0, (int) bufferSize));
      }
      vkUnmapMemory(getLogicalDevice(), stagingBufferMemory);

      createBuffer(
          bufferSize,
          VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
          VK_MEMORY_HEAP_DEVICE_LOCAL_BIT,
          pBuffer,
          pBufferMemory);

      indexBuffer = pBuffer.get(0);
      indexBufferMemory = pBufferMemory.get(0);

      copyBuffer(stagingBuffer, getIndexBuffer(), bufferSize);

      vkDestroyBuffer(getLogicalDevice(), stagingBuffer, null);
      vkFreeMemory(getLogicalDevice(), stagingBufferMemory, null);
    }
  }

  public abstract class IndexInfo<I> {
    private final List<I> indices;

    public IndexInfo(List<I> indices) {
      this.indices = indices;
    }

    public List<I> getIndices() {
      return indices;
    }

    protected abstract int getBufferSize();

    protected abstract void memCopy(ByteBuffer buffer);
  }

  public class ShortIndexInfo extends IndexInfo<Short> {
    public ShortIndexInfo(List<Short> indices) {
      super(indices);
    }

    @Override
    protected int getBufferSize() {
      return Short.BYTES * getIndices().size();
    }

    @Override
    protected void memCopy(ByteBuffer buffer) {
      getIndices().forEach(buffer::putShort);
      buffer.rewind();
    }
  }
}
