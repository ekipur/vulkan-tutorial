package ekipur.graphics.vulkan.tutorial.f_vertexbuffers;

import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.stream.IntStream;

import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.KHRSurface.*;
import static org.lwjgl.vulkan.VK10.*;

/**
 * This is an alternative version of the Staging Buffer Chapter, where I use a different queue
 * family for transfer operations.
 *
 * <p>As suggested in the tutorial, I will make the following modifications:
 *
 * <p>- Modify QueueFamilyIndices and findQueueFamilies to explicitly look for a queue family with
 * the VK_QUEUE_TRANSFER bit, but not the VK_QUEUE_GRAPHICS_BIT. - Modify createLogicalDevice to
 * request a handle to the transfer queue - Create a second command pool for command buffers that
 * are submitted on the transfer queue family - Change the sharingMode of resources to be
 * VK_SHARING_MODE_CONCURRENT and specify both the graphics and transfer queue families - Submit any
 * transfer commands like vkCmdCopyBuffer to the transfer queue instead of the graphics queue
 */
public final class Ch19StagingBufferTransferQueue extends Ch19StagingBuffer {
  private VkQueue transferQueue;
  private long transferCommandPool;
  private VkCommandBuffer transferCommandBuffer;
  private QueueTransferFamilyIndices familyIndices;

  public static void main(String[] args) {
    new Ch19StagingBufferTransferQueue().run();
  }

  public Ch19StagingBufferTransferQueue() {}

  @Override
  protected void extendBufferInfo(VkBufferCreateInfo bufferInfo) {
    logInfo(">> Ch19:extendBufferInfo (Ch19StagingBufferTransferQueue)");

    // Change the sharing mode to concurrent (it will be shared between graphics
    // and transfer queues)
    QueueTransferFamilyIndices queueFamilyProperties = findQueueFamilies(getPhysicalDevice());

    bufferInfo.pQueueFamilyIndices(
        MemoryStack.stackGet()
            .ints(queueFamilyProperties.getGraphicsFamily(), queueFamilyProperties.transferFamily));
    bufferInfo.sharingMode(VK_SHARING_MODE_CONCURRENT);
  }

  @Override
  protected void extendCreateCommandPool(
      VkCommandPoolCreateInfo poolInfo, LongBuffer pCommandPool) {
    logInfo(">> Ch19:extendCreateCommandPool (Ch19StagingBufferTransferQueue)");

    // ===> Create the transfer command pool <===
    poolInfo.queueFamilyIndex(familyIndices.transferFamily);
    // Tell Vulkan that the buffers of this pool will be constantly rerecorded
    poolInfo.flags(VK_COMMAND_POOL_CREATE_TRANSIENT_BIT);

    if (vkCreateCommandPool(getLogicalDevice(), poolInfo, null, pCommandPool) != VK_SUCCESS) {
      throw new RuntimeException("Failed to create command pool");
    }
    transferCommandPool = pCommandPool.get(0);

    allocateTransferCommandBuffer();
  }

  @Override
  protected void cleanupVertexBuffer() {
    vkFreeCommandBuffers(
        getLogicalDevice(), transferCommandPool, stackGet().pointers(transferCommandBuffer));
    vkDestroyCommandPool(getLogicalDevice(), transferCommandPool, null);

    super.cleanupVertexBuffer();
  }

  @Override
  protected QueueGraphicsFamilyIndices getFamilyIndices() {
    return familyIndices;
  }

  @Override
  protected void setQueue(VkQueue queue, int index, QueueGraphicsFamilyIndices indices) {
    if (indices instanceof QueueTransferFamilyIndices
        && index == ((QueueTransferFamilyIndices) indices).getTransferFamily()) {
      transferQueue = queue;
    }

    super.setQueue(queue, index, indices);
  }

  @Override
  protected QueueTransferFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
    if (familyIndices != null) {
      return familyIndices;
    }

    logInfo(">> Ch19:findQueueFamilies (Ch19StagingBufferTransferQueue)");

    familyIndices = new QueueTransferFamilyIndices();

    try (MemoryStack stack = stackPush()) {
      IntBuffer queueFamilyCount = stack.ints(0);

      vkGetPhysicalDeviceQueueFamilyProperties(device, queueFamilyCount, null);

      VkQueueFamilyProperties.Buffer queueFamilyProperties =
          VkQueueFamilyProperties.malloc(queueFamilyCount.get(0), stack);

      vkGetPhysicalDeviceQueueFamilyProperties(device, queueFamilyCount, queueFamilyProperties);
      IntBuffer presentSupport = stack.ints(VK_FALSE);

      for (int i = 0; i < queueFamilyProperties.capacity() || !familyIndices.isComplete(); i++) {
        if ((queueFamilyProperties.get(i).queueFlags() & VK_QUEUE_GRAPHICS_BIT) != 0) {
          familyIndices.setGraphicsFamily(i);
        } else if ((queueFamilyProperties.get(i).queueFlags() & VK_QUEUE_TRANSFER_BIT) != 0) {
          familyIndices.transferFamily = i;
        }
        // In case of having only 1 queueFamily, use the same index since
        // VK_QUEUE_GRAPHICS_BIT also implicitly
        // covers VK_QUEUE_TRANSFER_BIT
        if (queueFamilyProperties.capacity() == 1) {
          familyIndices.transferFamily = i;
        }

        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, getSurface(), presentSupport);

        if (presentSupport.get(0) == VK_TRUE) {
          familyIndices.setPresentFamily(i);
        }
      }
      return familyIndices;
    }
  }

  @Override
  protected void copyBuffer(long srcBuffer, long dstBuffer, long size) {
    logInfo(">> Ch19:copyBuffer (Ch19StagingBufferTransferQueue)");

    MemoryStack stack = stackGet();

    VkCommandBufferBeginInfo beginInfo =
        VkCommandBufferBeginInfo.calloc(stack)
            .sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO)
            .flags(VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);

    // Transfer command buffer implicitly reset
    vkBeginCommandBuffer(transferCommandBuffer, beginInfo);
    {
      VkBufferCopy.Buffer copyRegion = VkBufferCopy.calloc(1, stack);
      copyRegion.size(size);
      vkCmdCopyBuffer(transferCommandBuffer, srcBuffer, dstBuffer, copyRegion);
    }
    vkEndCommandBuffer(transferCommandBuffer);

    VkSubmitInfo submitInfo =
        VkSubmitInfo.calloc(stack)
            .sType(VK_STRUCTURE_TYPE_SUBMIT_INFO)
            .pCommandBuffers(stack.pointers(transferCommandBuffer));

    if (vkQueueSubmit(transferQueue, submitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
      throw new RuntimeException("Failed to submit copy command buffer");
    }

    vkQueueWaitIdle(transferQueue);
  }

  private void allocateTransferCommandBuffer() {
    try (MemoryStack stack = stackPush()) {
      VkCommandBufferAllocateInfo allocInfo =
          VkCommandBufferAllocateInfo.calloc(stack)
              .sType(VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO)
              .level(VK_COMMAND_BUFFER_LEVEL_PRIMARY)
              .commandPool(transferCommandPool)
              .commandBufferCount(1);

      PointerBuffer pCommandBuffer = stack.mallocPointer(1);
      vkAllocateCommandBuffers(getLogicalDevice(), allocInfo, pCommandBuffer);
      transferCommandBuffer = new VkCommandBuffer(pCommandBuffer.get(0), getLogicalDevice());
    }
  }

  private class QueueTransferFamilyIndices extends QueuePresentFamilyIndices {
    private Integer transferFamily;

    @Override
    protected boolean isComplete() {
      return super.isComplete() && transferFamily != null;
    }

    protected Integer getTransferFamily() {
      return transferFamily;
    }

    @Override
    public int[] unique() {
      return IntStream.of(getGraphicsFamily(), getPresentFamily(), transferFamily)
          .distinct()
          .toArray();
    }

    @Override
    public int[] array() {
      return new int[] {getGraphicsFamily(), getPresentFamily(), transferFamily};
    }
  }
}
