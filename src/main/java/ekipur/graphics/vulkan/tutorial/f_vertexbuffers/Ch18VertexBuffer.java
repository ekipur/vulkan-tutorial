package ekipur.graphics.vulkan.tutorial.f_vertexbuffers;

import java.nio.LongBuffer;
import java.util.ArrayList;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch18VertexBuffer extends Ch17VertexInputDescription {
  private final VertexInfo<?> vertexInfo;
  private long vertexBuffer;
  private long vertexBufferMemory;

  public static void main(String[] args) {
    new Ch18VertexBuffer().run();
  }

  public Ch18VertexBuffer() {
    vertexInfo =
        new VertexInfo<>(
            new ArrayList<>() {
              {
                add(new Vertex<>(new Vector2f(0.0f, -0.5f), new Vector3f(1.0f, 0.0f, 0.0f)));
                add(new Vertex<>(new Vector2f(0.5f, 0.5f), new Vector3f(0.0f, 1.0f, 0.0f)));
                add(new Vertex<>(new Vector2f(-0.5f, 0.5f), new Vector3f(0.0f, 0.0f, 1.0f)));
              }
            });
  }

  @Override
  protected VertexInfo<?> getVertexInfo() {
    return vertexInfo;
  }

  @Override
  protected void cleanupVertexBuffer() {
    logInfo("<< Ch18:cleanupVertexBuffer");

    cleanupBuffer(getVertexBuffer(), getVertexBufferMemory());
  }

  @Override
  protected void onCmdDraw(VkCommandBuffer commandBuffer, int commandBufferIndex) {
    logInfo(">> Ch18:onCmdDraw");

    bindVertexBuffer(commandBuffer);

    vkCmdDraw(commandBuffer, getVertexInfo().getVertices().size(), 1, 0, 0);
  }

  @Override
  protected void createVertexBuffer() {
    logInfo(">> Ch18:createVertexBuffer");

    try (MemoryStack stack = stackPush()) {
      int bufferSize = getVertexInfo().getVertexSize() * getVertexInfo().getVertices().size();
      LongBuffer pVertexBuffer = stack.mallocLong(1);
      LongBuffer pVertexBufferMemory = stack.mallocLong(1);

      createBuffer(
          bufferSize,
          VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
          pVertexBuffer,
          pVertexBufferMemory);

      setVertexBuffer(pVertexBuffer.get(0));
      setVertexBufferMemory(pVertexBufferMemory.get(0));
      PointerBuffer data = stack.mallocPointer(1);

      vkMapMemory(getLogicalDevice(), getVertexBufferMemory(), 0, bufferSize, 0, data);
      {
        getVertexInfo().memCopy(data.getByteBuffer(0, bufferSize));
      }
      vkUnmapMemory(getLogicalDevice(), getVertexBufferMemory());
    }
  }

  protected final long getVertexBuffer() {
    return vertexBuffer;
  }

  protected final void setVertexBuffer(long vertexBuffer) {
    this.vertexBuffer = vertexBuffer;
  }

  protected final long getVertexBufferMemory() {
    return vertexBufferMemory;
  }

  protected final void setVertexBufferMemory(long vertexBufferMemory) {
    this.vertexBufferMemory = vertexBufferMemory;
  }

  protected final void bindVertexBuffer(VkCommandBuffer commandBuffer) {
    MemoryStack stack = stackGet();
    LongBuffer vertexBuffers = stack.longs(getVertexBuffer());
    LongBuffer offsets = stack.longs(0);

    vkCmdBindVertexBuffers(commandBuffer, 0, vertexBuffers, offsets);
  }

  protected void extendBufferInfo(VkBufferCreateInfo bufferInfo) {
    logInfo(" > Ch18:extendBufferInfo");

    bufferInfo.sharingMode(VK_SHARING_MODE_EXCLUSIVE);
  }

  protected final void createBuffer(
      long size, int usage, int properties, LongBuffer pBuffer, LongBuffer pBufferMemory) {
    MemoryStack stack = stackGet();
    VkBufferCreateInfo bufferInfo =
        VkBufferCreateInfo.calloc(stack)
            .sType(VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO)
            .size(size)
            .usage(usage);

    extendBufferInfo(bufferInfo);

    if (vkCreateBuffer(getLogicalDevice(), bufferInfo, null, pBuffer) != VK_SUCCESS) {
      throw new RuntimeException("Failed to create vertex buffer");
    }

    VkMemoryRequirements memRequirements = VkMemoryRequirements.malloc(stack);
    vkGetBufferMemoryRequirements(getLogicalDevice(), pBuffer.get(0), memRequirements);

    VkMemoryAllocateInfo allocInfo =
        VkMemoryAllocateInfo.calloc(stack)
            .sType(VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO)
            .allocationSize(memRequirements.size())
            .memoryTypeIndex(findMemoryType(memRequirements.memoryTypeBits(), properties));

    if (vkAllocateMemory(getLogicalDevice(), allocInfo, null, pBufferMemory) != VK_SUCCESS) {
      throw new RuntimeException("Failed to allocate vertex buffer memory");
    }

    vkBindBufferMemory(getLogicalDevice(), pBuffer.get(0), pBufferMemory.get(0), 0);
  }

  protected final void cleanupBuffer(long buffer, long bufferMemory) {
    vkDestroyBuffer(getLogicalDevice(), buffer, null);
    vkFreeMemory(getLogicalDevice(), bufferMemory, null);
  }

  protected int findMemoryType(int typeFilter, int properties) {
    VkPhysicalDeviceMemoryProperties memProperties = VkPhysicalDeviceMemoryProperties.malloc();
    vkGetPhysicalDeviceMemoryProperties(getPhysicalDevice(), memProperties);

    for (int i = 0; i < memProperties.memoryTypeCount(); i++) {
      if ((typeFilter & 1 << i) != 0
          && (memProperties.memoryTypes(i).propertyFlags() & properties) == properties) {
        return i;
      }
    }

    throw new RuntimeException("Failed to find suitable memory type");
  }
}
