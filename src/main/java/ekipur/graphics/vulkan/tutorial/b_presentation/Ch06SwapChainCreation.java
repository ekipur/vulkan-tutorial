package ekipur.graphics.vulkan.tutorial.b_presentation;

import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import ekipur.graphics.vulkan.tutorial.a_setup.Ch05LogicalDevice;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static java.util.stream.Collectors.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.KHRSurface.*;
import static org.lwjgl.vulkan.KHRSwapchain.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch06SwapChainCreation extends Ch05LogicalDevice {
  private static final int UINT32_MAX = 0xFFFFFFFF;
  private final Set<String> deviceExtensions =
      Stream.of(VK_KHR_SWAPCHAIN_EXTENSION_NAME).collect(toSet());
  private VkQueue presentQueue;
  private long swapChain;
  private List<Long> swapChainImages;
  private int swapChainImageFormat;
  private VkExtent2D swapChainExtent;
  private QueuePresentFamilyIndices familyIndices;

  public static void main(String[] args) {
    new Ch06SwapChainCreation().run();
  }

  public Ch06SwapChainCreation() {}

  @Override
  public void initVulkan() {
    super.initVulkan();

    logDebug("=> Ch06:initVulkan");

    createSwapChain();
  }

  @Override
  public void cleanup() {
    cleanupSwapChain();

    super.cleanup();
  }

  @Override
  protected QueueGraphicsFamilyIndices getFamilyIndices() {
    return familyIndices;
  }

  @Override
  protected void setQueue(VkQueue queue, int index, QueueGraphicsFamilyIndices indices) {
    if (indices instanceof QueuePresentFamilyIndices
        && index == ((QueuePresentFamilyIndices) indices).getPresentFamily()) {
      presentQueue = queue;
    }

    super.setQueue(queue, index, indices);
  }

  @Override
  protected void createLogicalDevice() {
    logInfo(">> Ch06:createLogicalDevice");

    try (MemoryStack stack = stackPush()) {
      familyIndices = findQueueFamilies(getPhysicalDevice());
      int[] uniqueQueueFamilies = familyIndices.unique();

      VkDeviceQueueCreateInfo.Buffer queueCreateInfos =
          VkDeviceQueueCreateInfo.calloc(uniqueQueueFamilies.length, stack);

      for (int i = 0; i < uniqueQueueFamilies.length; i++) {
        VkDeviceQueueCreateInfo queueCreateInfo = queueCreateInfos.get(i);
        queueCreateInfo.sType(VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO);
        queueCreateInfo.queueFamilyIndex(uniqueQueueFamilies[i]);
        queueCreateInfo.pQueuePriorities(stack.floats(1.0f));
      }

      VkPhysicalDeviceFeatures deviceFeatures = VkPhysicalDeviceFeatures.calloc(stack);
      extendDeviceFeatures(deviceFeatures);

      VkDeviceCreateInfo createInfo = VkDeviceCreateInfo.calloc(stack);

      createInfo.sType(VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO);
      createInfo.pQueueCreateInfos(queueCreateInfos);
      // queueCreateInfoCount is automatically set

      createInfo.pEnabledFeatures(deviceFeatures);
      createInfo.ppEnabledExtensionNames(asPointerBuffer(deviceExtensions));

      if (isValidationLayersRequired()) {
        createInfo.ppEnabledLayerNames(asPointerBuffer(getValidationLayers()));
      }

      PointerBuffer pDevice = stack.pointers(VK_NULL_HANDLE);

      if (vkCreateDevice(getPhysicalDevice(), createInfo, null, pDevice) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create logical device");
      }

      setLogicalDevice(new VkDevice(pDevice.get(0), getPhysicalDevice(), createInfo));

      createQueues();
    }
  }

  @Override
  protected final boolean isDeviceSuitable(VkPhysicalDevice device) {
    logInfo(">> Ch06:isDeviceSuitable");

    QueuePresentFamilyIndices indices = findQueueFamilies(device);

    boolean extensionsSupported = checkDeviceExtensionSupport(device);
    boolean swapChainAdequate = false;

    if (extensionsSupported) {
      try (MemoryStack stack = stackPush()) {
        SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device, stack);
        swapChainAdequate =
            swapChainSupport.formats.hasRemaining() && swapChainSupport.presentModes.hasRemaining();
      }
    }

    return indices.isComplete()
        && extensionsSupported
        && swapChainAdequate
        && checkAnisotropySupported();
  }

  @Override
  protected QueuePresentFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
    if (familyIndices != null) {
      return familyIndices;
    }

    logInfo(">> Ch06:findQueueFamilies");

    QueuePresentFamilyIndices indices = new QueuePresentFamilyIndices();

    try (MemoryStack stack = stackPush()) {
      IntBuffer queueFamilyCount = stack.ints(0);

      vkGetPhysicalDeviceQueueFamilyProperties(device, queueFamilyCount, null);

      VkQueueFamilyProperties.Buffer queueFamilyProperties =
          VkQueueFamilyProperties.malloc(queueFamilyCount.get(0), stack);

      vkGetPhysicalDeviceQueueFamilyProperties(device, queueFamilyCount, queueFamilyProperties);
      IntBuffer presentSupport = stack.ints(VK_FALSE);

      for (int i = 0; i < queueFamilyProperties.capacity() || !indices.isComplete(); i++) {
        if ((queueFamilyProperties.get(i).queueFlags() & VK_QUEUE_GRAPHICS_BIT) != 0) {
          indices.setGraphicsFamily(i);
        }

        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, getSurface(), presentSupport);

        if (presentSupport.get(0) == VK_TRUE) {
          indices.setPresentFamily(i);
        }
      }

      return indices;
    }
  }

  protected final VkQueue getPresentQueue() {
    return presentQueue;
  }

  protected final void cleanupSwapChain() {
    logInfo(" < Ch06:cleanupSwapChain");

    vkDestroySwapchainKHR(getLogicalDevice(), swapChain, null);
  }

  protected final long getSwapChain() {
    return swapChain;
  }

  protected final List<Long> getSwapChainImages() {
    return swapChainImages;
  }

  protected final int getSwapChainImageFormat() {
    return swapChainImageFormat;
  }

  protected final VkExtent2D getSwapChainExtent() {
    return swapChainExtent;
  }

  protected final void createSwapChain() {
    logInfo(" > Ch06:createSwapChain");

    try (MemoryStack stack = stackPush()) {
      SwapChainSupportDetails swapChainSupport = querySwapChainSupport(getPhysicalDevice(), stack);

      VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
      int presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
      VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);

      IntBuffer imageCount = stack.ints(swapChainSupport.capabilities.minImageCount() + 1);

      if (swapChainSupport.capabilities.maxImageCount() > 0
          && imageCount.get(0) > swapChainSupport.capabilities.maxImageCount()) {
        imageCount.put(0, swapChainSupport.capabilities.maxImageCount());
      }

      VkSwapchainCreateInfoKHR createInfo = VkSwapchainCreateInfoKHR.calloc(stack);

      createInfo.sType(VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR);
      createInfo.surface(getSurface());

      // Image settings
      createInfo.minImageCount(imageCount.get(0));
      createInfo.imageFormat(surfaceFormat.format());
      createInfo.imageColorSpace(surfaceFormat.colorSpace());
      createInfo.imageExtent(extent);
      createInfo.imageArrayLayers(1);
      createInfo.imageUsage(VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);

      QueuePresentFamilyIndices indices = findQueueFamilies(getPhysicalDevice());

      if (!indices.getGraphicsFamily().equals(indices.getPresentFamily())) {
        createInfo.imageSharingMode(VK_SHARING_MODE_CONCURRENT);
        createInfo.pQueueFamilyIndices(
            stack.ints(indices.getGraphicsFamily(), indices.presentFamily));
      } else {
        createInfo.imageSharingMode(VK_SHARING_MODE_EXCLUSIVE);
      }

      createInfo.preTransform(swapChainSupport.capabilities.currentTransform());
      createInfo.compositeAlpha(VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR);
      createInfo.presentMode(presentMode);
      createInfo.clipped(true);

      createInfo.oldSwapchain(VK_NULL_HANDLE);

      LongBuffer pSwapChain = stack.longs(VK_NULL_HANDLE);

      if (vkCreateSwapchainKHR(getLogicalDevice(), createInfo, null, pSwapChain) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create swap chain");
      }

      swapChain = pSwapChain.get(0);

      vkGetSwapchainImagesKHR(getLogicalDevice(), swapChain, imageCount, null);

      LongBuffer pSwapchainImages = stack.mallocLong(imageCount.get(0));

      vkGetSwapchainImagesKHR(getLogicalDevice(), swapChain, imageCount, pSwapchainImages);

      swapChainImages = new ArrayList<>(imageCount.get(0));

      for (int i = 0; i < pSwapchainImages.capacity(); i++) {
        swapChainImages.add(pSwapchainImages.get(i));
      }

      swapChainImageFormat = surfaceFormat.format();
      swapChainExtent = VkExtent2D.create().set(extent);
    }
  }

  protected boolean checkAnisotropySupported() {
    return true; // dont let isDeviceSuitable fail in this chapter
  }

  protected void extendDeviceFeatures(VkPhysicalDeviceFeatures deviceFeatures) {}

  private VkSurfaceFormatKHR chooseSwapSurfaceFormat(VkSurfaceFormatKHR.Buffer availableFormats) {
    return availableFormats.stream()
        .filter(availableFormat -> availableFormat.format() == VK_FORMAT_B8G8R8_UNORM)
        .filter(
            availableFormat -> availableFormat.colorSpace() == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        .findAny()
        .orElse(availableFormats.get(0));
  }

  private int chooseSwapPresentMode(IntBuffer availablePresentModes) {
    for (int i = 0; i < availablePresentModes.capacity(); i++) {
      if (availablePresentModes.get(i) == VK_PRESENT_MODE_MAILBOX_KHR) {
        return availablePresentModes.get(i);
      }
    }

    return VK_PRESENT_MODE_FIFO_KHR;
  }

  private VkExtent2D chooseSwapExtent(VkSurfaceCapabilitiesKHR capabilities) {
    if (capabilities.currentExtent().width() != UINT32_MAX) {
      return capabilities.currentExtent();
    }

    IntBuffer width = stackGet().ints(0);
    IntBuffer height = stackGet().ints(0);

    glfwGetFramebufferSize(getWindowHandle(), width, height);

    VkExtent2D actualExtent = VkExtent2D.malloc().set(width.get(0), height.get(0));

    VkExtent2D minExtent = capabilities.minImageExtent();
    VkExtent2D maxExtent = capabilities.maxImageExtent();

    actualExtent.width(clamp(minExtent.width(), maxExtent.width(), actualExtent.width()));
    actualExtent.height(clamp(minExtent.height(), maxExtent.height(), actualExtent.height()));

    return actualExtent;
  }

  private int clamp(int min, int max, int value) {
    return Math.max(min, Math.min(max, value));
  }

  private boolean checkDeviceExtensionSupport(VkPhysicalDevice device) {
    try (MemoryStack stack = stackPush()) {
      IntBuffer extensionCount = stack.ints(0);

      vkEnumerateDeviceExtensionProperties(device, (String) null, extensionCount, null);

      VkExtensionProperties.Buffer availableExtensions =
          VkExtensionProperties.malloc(extensionCount.get(0), stack);

      vkEnumerateDeviceExtensionProperties(
          device, (String) null, extensionCount, availableExtensions);

      return availableExtensions.stream()
          .map(VkExtensionProperties::extensionNameString)
          .collect(toSet())
          .containsAll(deviceExtensions);
    }
  }

  private SwapChainSupportDetails querySwapChainSupport(
      VkPhysicalDevice device, MemoryStack stack) {
    SwapChainSupportDetails details = new SwapChainSupportDetails();

    details.capabilities = VkSurfaceCapabilitiesKHR.malloc(stack);
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, getSurface(), details.capabilities);

    IntBuffer count = stack.ints(0);

    vkGetPhysicalDeviceSurfaceFormatsKHR(device, getSurface(), count, null);

    if (count.get(0) != 0) {
      details.formats = VkSurfaceFormatKHR.malloc(count.get(0), stack);
      vkGetPhysicalDeviceSurfaceFormatsKHR(device, getSurface(), count, details.formats);
    }

    vkGetPhysicalDeviceSurfacePresentModesKHR(device, getSurface(), count, null);

    if (count.get(0) != 0) {
      details.presentModes = stack.mallocInt(count.get(0));
      vkGetPhysicalDeviceSurfacePresentModesKHR(device, getSurface(), count, details.presentModes);
    }

    return details;
  }

  protected class QueuePresentFamilyIndices extends QueueGraphicsFamilyIndices {
    // We use Integer to use null as the empty value
    private Integer presentFamily;

    @Override
    protected boolean isComplete() {
      return super.isComplete() && presentFamily != null;
    }

    public final void setPresentFamily(Integer family) {
      presentFamily = family;
    }

    protected final Integer getPresentFamily() {
      return presentFamily;
    }

    @Override
    public int[] unique() {
      return IntStream.of(getGraphicsFamily(), presentFamily).distinct().toArray();
    }

    public int[] array() {
      return new int[] {getGraphicsFamily(), presentFamily};
    }
  }

  private class SwapChainSupportDetails {
    private VkSurfaceCapabilitiesKHR capabilities;
    private VkSurfaceFormatKHR.Buffer formats;
    private IntBuffer presentModes;
  }
}
