package ekipur.graphics.vulkan.tutorial.b_presentation;

import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkImageViewCreateInfo;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch07SwapChainImageViews extends Ch06SwapChainCreation {
  private List<Long> swapChainImageViews;

  public static void main(String[] args) {
    new Ch07SwapChainImageViews().run();
  }

  public Ch07SwapChainImageViews() {}

  @Override
  public void initVulkan() {
    super.initVulkan();

    logDebug("=> Ch07:initVulkan");

    createSwapChainImageViews();
  }

  @Override
  public void cleanup() {
    cleanupSwapChainImageViews();

    super.cleanup();
  }

  protected final void cleanupSwapChainImageViews() {
    logInfo(" < Ch07:cleanupSwapChainImageViews");

    swapChainImageViews.forEach(
        imageView -> vkDestroyImageView(getLogicalDevice(), imageView, null));
  }

  protected final List<Long> getSwapChainImageViews() {
    return swapChainImageViews;
  }

  protected void setImageViewComponents(VkImageViewCreateInfo info) {
    logInfo(" > Ch07:setImageViewComponents (default component swizzles)");

    info.components().r(VK_COMPONENT_SWIZZLE_IDENTITY);
    info.components().g(VK_COMPONENT_SWIZZLE_IDENTITY);
    info.components().b(VK_COMPONENT_SWIZZLE_IDENTITY);
    info.components().a(VK_COMPONENT_SWIZZLE_IDENTITY);
  }

  protected int getMipLevels() {
    return 1;
  }

  protected final long createImageView(long image, int format, int aspectFlags) {
    try (MemoryStack stack = stackPush()) {
      VkImageViewCreateInfo viewInfo = VkImageViewCreateInfo.calloc(stack);
      viewInfo.sType(VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO);
      viewInfo.image(image);
      viewInfo.viewType(VK_IMAGE_VIEW_TYPE_2D);
      viewInfo.format(format);
      viewInfo
          .subresourceRange()
          .aspectMask(aspectFlags)
          .baseMipLevel(0)
          .levelCount(getMipLevels())
          .baseArrayLayer(0)
          .layerCount(1);

      setImageViewComponents(viewInfo);

      LongBuffer pImageView = stack.mallocLong(1);

      if (vkCreateImageView(getLogicalDevice(), viewInfo, null, pImageView) != VK_SUCCESS) {
        throw new RuntimeException("Failed to create texture image view");
      }

      return pImageView.get(0);
    }
  }

  protected final void createSwapChainImageViews() {
    logInfo(" > Ch07:createSwapChainImageViews");

    List<Long> swapchainImages = getSwapChainImages();
    swapChainImageViews = new ArrayList<>(swapchainImages.size());

    for (long swapChainImage : swapchainImages) {
      swapChainImageViews.add(
          createImageView(swapChainImage, getSwapChainImageFormat(), VK_IMAGE_ASPECT_COLOR_BIT));
    }
  }
}
