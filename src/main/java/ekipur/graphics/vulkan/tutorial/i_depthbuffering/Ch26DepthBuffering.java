package ekipur.graphics.vulkan.tutorial.i_depthbuffering;

import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.ArrayList;
import java.util.function.BiConsumer;

import ekipur.graphics.vulkan.tutorial.h_texturemapping.Ch25TextureMapping;
import ekipur.graphics.vulkan.tutorial.naitsirc98.ShaderSPIRVUtils.ShaderKind;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import static ekipur.graphics.vulkan.tutorial.TutorialBase.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.vulkan.VK10.*;

public class Ch26DepthBuffering extends Ch25TextureMapping {
  private final VertexInfo<?> vertexInfo;
  private final ShortIndexInfo indexInfo;
  private ImageResource depthResource;

  public static void main(String[] args) {
    new Ch26DepthBuffering().run();
  }

  public Ch26DepthBuffering() {
    vertexInfo =
        new VertexInfo<>(
            new ArrayList<>() {
              {
                add(
                    new VertexExt<>(
                        new Vector3f(-0.5f, -0.5f, 0.0f),
                        new Vector3f(1.0f, 0.0f, 0.0f),
                        new Vector2f(0.0f, 0.0f)));
                add(
                    new VertexExt<>(
                        new Vector3f(0.5f, -0.5f, 0.0f),
                        new Vector3f(0.0f, 1.0f, 0.0f),
                        new Vector2f(1.0f, 0.0f)));
                add(
                    new VertexExt<>(
                        new Vector3f(0.5f, 0.5f, 0.0f),
                        new Vector3f(0.0f, 0.0f, 1.0f),
                        new Vector2f(1.0f, 1.0f)));
                add(
                    new VertexExt<>(
                        new Vector3f(-0.5f, 0.5f, 0.0f),
                        new Vector3f(1.0f, 1.0f, 1.0f),
                        new Vector2f(0.0f, 1.0f)));

                add(
                    new VertexExt<>(
                        new Vector3f(-0.5f, -0.5f, -0.5f),
                        new Vector3f(1.0f, 0.0f, 0.0f),
                        new Vector2f(0.0f, 0.0f)));
                add(
                    new VertexExt<>(
                        new Vector3f(0.5f, -0.5f, -0.5f),
                        new Vector3f(0.0f, 1.0f, 0.0f),
                        new Vector2f(1.0f, 0.0f)));
                add(
                    new VertexExt<>(
                        new Vector3f(0.5f, 0.5f, -0.5f),
                        new Vector3f(0.0f, 0.0f, 1.0f),
                        new Vector2f(1.0f, 1.0f)));
                add(
                    new VertexExt<>(
                        new Vector3f(-0.5f, 0.5f, -0.5f),
                        new Vector3f(1.0f, 1.0f, 1.0f),
                        new Vector2f(0.0f, 1.0f)));
              }
            });

    indexInfo =
        new ShortIndexInfo(
            new ArrayList<>() {
              {
                add((short) 2);
                add((short) 1);
                add((short) 0);
                add((short) 0);
                add((short) 3);
                add((short) 2);
                add((short) 6);
                add((short) 5);
                add((short) 4);
                add((short) 4);
                add((short) 7);
                add((short) 6);
              }
            });
  }

  @Override
  protected VertexInfo<?> getVertexInfo() {
    return vertexInfo;
  }

  @Override
  protected IndexInfo<?> getIndexInfo() {
    return indexInfo;
  }

  @Override
  protected final void createFramebuffers() {
    logDebug("=> Ch26:createFramebuffers");

    createDepthResources();

    super.createFramebuffers();
  }

  @Override
  protected void cleanupUniformBuffers() {
    logDebug("=< Ch26:cleanupUniformBuffers");

    depthResource.cleanup();

    super.cleanupUniformBuffers();
  }

  @Override
  protected LongBuffer allocateFramebufferAttachments(MemoryStack stack) {
    return stack.longs(VK_NULL_HANDLE, depthResource.getImageView());
  }

  @Override
  protected void extendGraphicsPipline(
      MemoryStack stack, GraphicsPipelineElements pipelineElements) {
    if (pipelineElements.shaderSources.isEmpty()) {
      logInfo("=> Ch26:extendGraphicsPipeline (26_shader_depth, depth-stencil)");

      pipelineElements.shaderSources.put(
          ShaderKind.VERTEX_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/26_shader_depth.vert");

      pipelineElements.shaderSources.put(
          ShaderKind.FRAGMENT_SHADER,
          "ekipur/graphics/vulkan/tutorial/resources/naitsirc98/shaders/26_shader_depth.frag");
    } else {
      logInfo("=> Ch26:extendGraphicsPipeline (depth-stencil)");
    }

    pipelineElements
        .depthStencil
        .sType(VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO)
        .depthTestEnable(true)
        .depthWriteEnable(true)
        .depthCompareOp(VK_COMPARE_OP_LESS)
        .depthBoundsTestEnable(false)
        .minDepthBounds(0.0f) // Optional
        .maxDepthBounds(1.0f) // Optional
        .stencilTestEnable(false);

    super.extendGraphicsPipline(stack, pipelineElements);
  }

  @Override
  protected VkClearValue.Buffer onRenderPassInfoClearValues(MemoryStack stack) {
    logInfo(">> Ch26:onRenderPassInfoClearValues");

    VkClearValue.Buffer clearValues = VkClearValue.calloc(2, stack);
    clearValues.get(0).color().float32(stack.floats(0.0f, 0.0f, 0.0f, 1.0f));
    clearValues.get(1).depthStencil().set(1.0f, 0);
    return clearValues;
  }

  @Override
  protected int getRenderPassAttachmentCount() {
    return super.getRenderPassAttachmentCount() + 1;
  }

  @Override
  protected void createRenderPassAttachment(
      int index,
      VkAttachmentDescription attachment,
      VkAttachmentReference attachmentRef,
      VkSubpassDescription.Buffer subpass,
      MemoryStack stack) {
    if (index == 1) {
      logInfo("-> Ch26:createRenderPassAttachment " + index + " (depth-stencil)");

      attachment
          .format(findDepthFormat())
          .samples(getDepthResourcesNumSamples())
          .loadOp(VK_ATTACHMENT_LOAD_OP_CLEAR)
          .storeOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
          .stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE)
          .stencilStoreOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
          .initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
          .finalLayout(VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

      VkAttachmentReference depthAttachmentRef =
          VkAttachmentReference.calloc(stack)
              .attachment(index)
              .layout(VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

      subpass.pDepthStencilAttachment(depthAttachmentRef);
    } else {
      super.createRenderPassAttachment(index, attachment, attachmentRef, subpass, stack);
    }
  }

  @Override
  protected void onImageMemoryBarrierPrecondition(
      VkImageMemoryBarrier.Buffer barrier, int format, int oldLayout, int newLayout) {
    logInfo(">> Ch26:onImageMemoryBarrierPrecondition");

    if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
      barrier.subresourceRange().aspectMask(VK_IMAGE_ASPECT_DEPTH_BIT);

      if (format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT) {
        barrier
            .subresourceRange()
            .aspectMask(barrier.subresourceRange().aspectMask() | VK_IMAGE_ASPECT_STENCIL_BIT);
      }
    } else {
      barrier.subresourceRange().aspectMask(VK_IMAGE_ASPECT_COLOR_BIT);
    }
  }

  @Override
  protected boolean onImageMemoryBarrierPostcondition(
      VkImageMemoryBarrier.Buffer barrier,
      int oldLayout,
      int newLayout,
      BiConsumer<Integer, Integer> stageMaskConsumer) {
    boolean consumed =
        super.onImageMemoryBarrierPostcondition(barrier, oldLayout, newLayout, stageMaskConsumer);

    if (!consumed
        && oldLayout == VK_IMAGE_LAYOUT_UNDEFINED
        && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
      logDebug("=> Ch26:onImageMemoryBarrierPostcondition");

      barrier
          .srcAccessMask(0)
          .dstAccessMask(
              VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT
                  | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT);

      stageMaskConsumer.accept(
          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT);
      consumed = true;
    }
    return consumed;
  }

  protected final ImageResource getDepthResource() {
    return depthResource;
  }

  protected int getDepthResourcesNumSamples() {
    return VK_SAMPLE_COUNT_1_BIT;
  }

  protected void createDepthResources() {
    logInfo(" > Ch26:createDepthResources");

    try (MemoryStack stack = stackPush()) {
      int depthFormat = findDepthFormat();

      LongBuffer pDepthImage = stack.mallocLong(1);
      LongBuffer pDepthImageMemory = stack.mallocLong(1);

      createImage(
          getSwapChainExtent().width(),
          getSwapChainExtent().height(),
          getDepthResourcesNumSamples(),
          depthFormat,
          VK_IMAGE_TILING_OPTIMAL,
          VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
          pDepthImage,
          pDepthImageMemory);

      long depthImage = pDepthImage.get(0);
      long depthImageMemory = pDepthImageMemory.get(0);

      depthResource =
          new ImageResource(
              depthImage,
              depthImageMemory,
              createImageView(depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT));

      // Explicitly transitioning the depth depthResource
      transitionImageLayout(
          depthImage,
          depthFormat,
          VK_IMAGE_LAYOUT_UNDEFINED,
          VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
    }
  }

  private int findSupportedFormat(IntBuffer formatCandidates, int tiling, int features) {
    try (MemoryStack stack = stackPush()) {
      VkFormatProperties props = VkFormatProperties.calloc(stack);

      for (int i = 0; i < formatCandidates.capacity(); ++i) {
        int format = formatCandidates.get(i);

        vkGetPhysicalDeviceFormatProperties(getPhysicalDevice(), format, props);

        if ((tiling == VK_IMAGE_TILING_LINEAR
                && (props.linearTilingFeatures() & features) == features)
            || (tiling == VK_IMAGE_TILING_OPTIMAL
                && (props.optimalTilingFeatures() & features) == features)) {
          return format;
        }
      }
    }

    throw new RuntimeException("Failed to find supported format");
  }

  protected final int findDepthFormat() {
    return findSupportedFormat(
        MemoryStack.stackGet()
            .ints(VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT),
        VK_IMAGE_TILING_OPTIMAL,
        VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
  }

  public class ImageResource {
    private final long image;
    private final long imageMemory;
    private final long imageView;

    public ImageResource(long image, long imageMemory, long imageView) {
      this.image = image;
      this.imageMemory = imageMemory;
      this.imageView = imageView;
    }

    public long getImage() {
      return image;
    }

    public long getImageMemory() {
      return imageMemory;
    }

    public long getImageView() {
      return imageView;
    }

    public final void cleanup() {
      logInfo(" < cleanupImageResource");

      vkDestroyImageView(getLogicalDevice(), getImageView(), null);
      vkDestroyImage(getLogicalDevice(), getImage(), null);
      vkFreeMemory(getLogicalDevice(), getImageMemory(), null);
    }
  }
}
